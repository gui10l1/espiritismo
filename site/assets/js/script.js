$(document).ready(function() {
	$("#search").click(function(){
		$("#search-form").css({
			display: 'block'
		})
	})

	$("#dismiss").click(function(){
		$("#search-form").css({
			display: 'none'
		})
	})

	$("#dismiss-menu").click(function(){
		$("#aside-menu").css({
			display: 'none'
		})
	})

	$("#menu-icon").click(function(){
		$("#aside-menu").css({
			display: 'block'
		})
	})

	$(window).scroll(function(){
		const element = $("#share-post")
		if($(this).scrollTop() > 1293 && $(this).scrollTop() < 2550) {
			element.addClass('fixed-to-top')
		} else if($(this).scrollTop() > 2550) {
			element.addClass('fixed-to')
			element.removeClass('fixed-to-top')
		} else if ($(this).scrollTop() < 1293){
			element.removeClass('fixed-to-top')
			element.removeClass('fixed-to')
		}
	})

	$("#comment").click(function () {
		if($("#form-comment").hasClass('d-none')) {
			$("#form-comment").removeClass('d-none');
			$("#form-comment").hide().fadeIn(400)
			$("#to-rotate").removeClass('rotate-0').addClass('rotate-180')
		} else {
			$("#form-comment").fadeOut(100, function() {
				$("#form-comment").addClass('d-none');
			})
			$("#to-rotate").removeClass('rotate-180').addClass('rotate-0')
		}
	})
}); 
"use strict"

$(document).ready(function(){
    const form = document.getElementById('form')

	$("#search").click(function(){
		$("#section-search").css({
			display: 'block'
		})
	})

	$("#dismiss").click(function(){
		$("#section-search").css({
			display: 'none'
		})
	})

	$("#search-mobile").click(function(){
		$("#section-search").css({
			display: 'block'
		})
	})

	$("#dismiss").click(function(){
		$("#section-search").css({
			display: 'none'
		})
	})

	$("#nav-icon").click(function(){
		const id = $(this).attr("data-target")

		const element = document.getElementById(id);

		if(getComputedStyle(element).display === "none"){
			$("#"+id).css({
				display: 'block'
			})
		} else {
			$("#"+id).css({
				display: 'none'
			})
		}
	})

	$("#back-to-top").click(function(){
		$("body, html").animate({
			scrollTop: 0
		}, 500)
	})

    //--------------------------------------------------------------VALIDATIONS

    function validateIfIsNull(inputs) {
        for (const i of inputs) {
            if($("#" + i.name).val() === "") {
                Swal.fire({
                    icon: 'error',
                    title: 'Campo(s) vazio(s)!',
                    text: 'Preencha todos os campos para continuar!'
                })

                return false
            }
        }

        return true
    }

    function validateLogin(){
        if(document.getElementById('email') !== null){
            if($("#email").val().indexOf("@") < 0 || $("#email").val().indexOf(".") < 0) {
                Swal.fire({
                    icon: 'error',
                    title: 'Email inválido!',
                    text: 'Insira um email correto!'
                })

                return false
            }
        }

        if(document.getElementById('password') !== null) {
            if($("#password").val().length < 8 || $("#password").val().length > 16) {
                Swal.fire({
                    icon: 'error',
                    title: 'Senha inválida!',
                    text: 'A senha deve conter entre 8 e 16 caracteres!'
                })

                return false
            }
        }

        return true
    }

    $("#password").keyup(function () {
        const password = $("#password").val();

        if (password.length > 16 || password.length < 8) {
            $("#password-strength").css({
                width: '0%'
            });
            $("#password-strength").removeClass('bg-success').addClass('bg-danger');
            $("#message-password").html('A senha deve conter entre 8 e 16 caracteres!').addClass('text-danger');
            return;
        } else {
            $("#password-strength").css({
                width: '25%'
            });
            $("#message-password").html('').addClass('text-danger');
        }

        if (password.match(/([a-z])/) && password.match(/([A-Z])/)) {
            $("#password-strength").css({
                width: '50%'
            });
        } else {
            $("#password-strength").css({
                width: '25%'
            });
            $("#password-strength").removeClass('bg-success').addClass('bg-danger');
            $("#message-password").html('A senha deve conter letras maiúsculas e minúsculas!').addClass('text-danger');
            return;
        }

        if (password.match(/([0-9])/)) {
            $("#password-strength").css({
                width: '75%'
            });
        } else {
            $("#password-strength").css({
                width: '50%'
            });
            $("#password-strength").removeClass('bg-success').addClass('bg-danger');
            $("#message-password").html('A senha deve conter números!').addClass('text-danger');
            return;
        }

        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
            $("#password-strength").css({
                width: '100%'
            });
            $("#password-strength").removeClass('bg-danger').addClass('bg-success');
        } else {
            $("#password-strength").css({
                width: '75%'
            });
            $("#password-strength").removeClass('bg-success').addClass('bg-danger');
            $("#message-password").html('A senha deve conter ao menos 1 caractere especial!').addClass('text-danger');
        }
    });

    $("#rep_password").keyup(function () {
        const password = $("#password").val();
        const rep_password = $("#rep_password").val();

        if (password.length > 16 || password.length < 8) {
            $("#validate-new-user").prop('disabled', true);
            $("#validate-edit-user").prop('disabled', true);
            $("#validate-reset-password").prop('disabled', true);
            return
        } else {
            $("#validate-new-user").prop('disabled', false);
            $("#validate-edit-user").prop('disabled', false);
            $("#validate-reset-password").prop('disabled', false);

        }

        if (password.match(/([a-z])/) && password.match(/([A-Z])/)) {
            $("#validate-new-user").prop('disabled', false);
            $("#validate-edit-user").prop('disabled', false);
            $("#validate-reset-password").prop('disabled', false);

        } else {
            $("#validate-new-user").prop('disabled', true);
            $("#validate-edit-user").prop('disabled', true);
            $("#validate-reset-password").prop('disabled', true);

        }

        if (password.match(/([0-9])/)) {
            $("#validate-new-user").prop('disabled', false);
            $("#validate-edit-user").prop('disabled', false);
            $("#validate-reset-password").prop('disabled', false);

        } else {
            $("#validate-new-user").prop('disabled', true);
            $("#validate-edit-user").prop('disabled', true);
            $("#validate-reset-password").prop('disabled', true);

            return;
        }

        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
            $("#validate-new-user").prop('disabled', false);
            $("#validate-edit-user").prop('disabled', false);
            $("#validate-reset-password").prop('disabled', false);
        } else {
            $("#validate-new-user").prop('disabled', true);
            $("#validate-edit-user").prop('disabled', true);
            $("#validate-reset-password").prop('disabled', true);
            return;
        }

        if (password === "") {
            $("#validate-new-user").prop('disabled', true);
            $("#validate-edit-user").prop('disabled', true);
            $("#validate-reset-password").prop('disabled', true);
            $("#message-rep-password").html('Insira uma senha primeiro!').addClass('text-danger');
            return;
        } else {
            $("#message-rep-password").html('');
            $("#validate-new-user").prop('disabled', false);
            $("#validate-edit-user").prop('disabled', false);
            $("#validate-reset-password").prop('disabled', false);
        }

        if (password !== rep_password) {
            $("#validate-new-user").prop('disabled', true);
            $("#validate-edit-user").prop('disabled', true);
            $("#validate-reset-password").prop('disabled', true);
            $("#message-rep-password").html('As senhas não conferem!').addClass('text-danger');
            return;
        } else {
            $("#message-rep-password").html('As senhas conferem!').removeClass('text-danger').addClass('text-success');
        }
        $("#validate-new-user").prop('disabled', false);
        $("#validate-edit-user").prop('disabled', false);
        $("#validate-reset-password").prop('disabled', false);
    })

    $("#rep_email").keyup(function () {
        const email = $("#email").val();
        const rep_email = $("#rep_email").val();

        if (email === "") {
            $("#validate-new-user").prop('disabled', true);
            $("#validate-edit-user").prop('disabled', true);
            $("#message-rep-email").html('Insira um email primeiro!').addClass('text-danger');
            return;
        } else {
            $("#message-rep-email").html('');
            $("#validate-new-user").prop('disabled', false);
            $("#validate-edit-user").prop('disabled', false);
        }

        if (email !== rep_email) {
            $("#validate-new-user").prop('disabled', true);
            $("#validate-edit-user").prop('disabled', true);
            $("#message-rep-email").html('O email não confere!').addClass('text-danger');
            return;
        } else {
            $("#message-rep-email").html('O email confere!').removeClass('text-danger').addClass('text-success');
        }
        $("#validate-new-user").prop('disabled', false);
        $("#validate-edit-user").prop('disabled', false);
    });

    $("#rep_email").on("copy paste cut", function (e) {
        e.preventDefault();
    });

    $("#rep_password").on("copy paste cut", function (e) {
        e.preventDefault();
    });

    //--------------------------------------------------------------BUTTONS

    $("#validate-login").click(function () {
        const inputs = $("#" + form.id).serializeArray()

        if(!validateIfIsNull(inputs)) {
            return
        }

        if (!validateLogin()) {
            return
        }

        form.submit()
    })

    $("#validate-new-user").click(function() {
        const inputs = $("#" + form.id).serializeArray()

        if (!validateIfIsNull(inputs)) {
            return
        }

        if (!validateLogin()) {
            return
        }

        if ($("#user_photo").val() === "") {
            Swal.fire({
                icon: 'error',
                title: 'Foto obrigatória',
                text: 'Insira um arquivo de foto para o usuário!'
            })

            return
        }

        form.submit()
    })

    $("#validate-edit-user").click(function () {
        const inputs = $(`#${form.id}`).serializeArray()

        if (!validateIfIsNull(inputs)) {
            return
        }

        form.submit()

    })

    $("#validate-reset-password").click(function () {
        const inputs = $("#" + form.id).serializeArray()

        if (!validateIfIsNull(inputs)) {
            return
        }

        form.submit()

    })

    //---------------------------------------------------------------DRAG 'N DROP

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview-user-photo').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }

    $("#user_photo_preview").change(function () {
        var ext = $(this).val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg']) == -1) {
            Swal.fire({
                icon: 'error',
                title: 'Arquivo inválido!',
                text: 'O arquivo deve ser uma imagem (png, jpg)!'
            });

            this.value = '';

            return;
        }

        readURL(this);
    });

    $("#user_photo").change(function () {

        const ext = $(this).val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg']) == -1) {
            Swal.fire({
                icon: 'error',
                title: 'Arquivo inválido!',
                text: 'O arquivo deve ser uma imagem (png, jpg)!'
            });

            this.value = '';

            return;
        }

        readURL(this);

        if ($("#drag-and-drop").hasClass('col-md-12')) {
            $("#drag-and-drop").removeClass('col-md-12').addClass('col-md-6');
            $("#p-photo").removeClass('d-none');
            $("#drag-n-drop").addClass('d-none')
        }

        $("#file-name").html(this.value)
    });

    console.log(true)



    //------------------------------------------------------------------DATATABLES

    if(document.getElementById('table') != null){
        $("#table").DataTable({
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "Mostrar _MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                },
                "select": {
                    "rows": {
                        "_": "Selecionado %d linhas",
                        "0": "Nenhuma linha selecionada",
                        "1": "Selecionado 1 linha"
                    }
                },
                "buttons": {
                    "copy": "Copiar para a área de transferência",
                    "copyTitle": "Cópia bem sucedida",
                    "copySuccess": {
                        "1": "Uma linha copiada com sucesso",
                        "_": "%d linhas copiadas com sucesso"
                    }
                }
            },
            "lengthMenu": [[10, 20, 30], [10, 20, 30]]
        });
    }


    //-------------------------------------------------------------------------------------------DESTROY BUTTON

    $(".destroy").click(function () {
        Swal.fire({
            title: 'Deseja mesmo prosseguir com esta ação?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Prosseguir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.value) {
                const a = $(this).find("a");
                for (const aa of a) {
                    return window.location.href = aa.href
                }
            }
        })
    });
})

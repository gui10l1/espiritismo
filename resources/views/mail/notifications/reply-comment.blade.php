<div style="background: #f1f1f1">
    <h1>Um usuário respondeu a um comentário feito por você no {{ env('APP_NAME') }}!</h1>

    <p>
        Clique no link abaixo para ler a resposta feita em seu comentário!
    </p>

    <a href="{{ $route }}">Clique aqui</a>
</div>


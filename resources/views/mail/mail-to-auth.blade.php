<div style="background: #f1f1f1">
    <h1>Bem-Vindo {{ $user->NM_USER }}!</h1>

    <p>
        Clique no link abaixo para ativar sua conta recém cadastrada!
    </p>

    <a href="{{ $route }}">Clique aqui</a>
</div>


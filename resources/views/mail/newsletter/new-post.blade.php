<div style="background: #f1f1f1">
    <h1>Há uma nova postagem a ser lida no {{ env('APP_NAME') }}!</h1>

    <p>
        Clique no link abaixo para visualizar a postagem!
    </p>

    <a href="{{ $route }}">Clique aqui</a>
</div>


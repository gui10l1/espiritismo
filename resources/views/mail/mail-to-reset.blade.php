<div style="background: #f1f1f1">
    <h1>Olá {{ $user->NM_USER }}!</h1>

    <p>
        Clique no link abaixo para resetar sua senha!
    </p>

    <a href="{{ $route }}">Clique aqui</a>
</div>

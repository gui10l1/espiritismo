@extends('layouts.app')
@section('content')
<div class="card">
    <div class="card-header">
        Nova imagem
    </div>
    <div class="card-body">
        <div class="alert alert-primary text-center">
            Tenha preferências com imagens no estilo retrato
        </div>
        <form action="{{ route('dashboard.image.store') }}" method="POST" id="form" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="form-group col-md-12" id="drag-and-drop">
                    <div class="dropzone">
                        <div class="dropzone-content">
                            <span id="drag-n-drop">Arraste o arquivo para cá</span>
                            <strong><span id="file-name"></span></strong>
                            <input type="file" name="image_url" id="image_url" class="input-file-custom" required>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row d-none text-center" id="preview">
                <div class="form-group col-md-12">
                    <label>Pré visualização</label>
                    <br>
                    <img src="" id="preview-user-photo" style="max-width: 1fr; height: 100%; border-radius: 15px">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <button class="btn btn-success">Salvar</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

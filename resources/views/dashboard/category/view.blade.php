@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            Editar Categoria
        </div>

        <div class="card-body">
            <form action="{{ route('dashboard.category.edit', \Illuminate\Support\Facades\Crypt::encryptString($category->ID_CATEGORY)) }}" method="POST" id="form">
                @csrf
                <div class="row">
                    <div class="col-md-6 col-sm-6 form-group">
                        <label>Nome</label>
                        <input type="text" name="nm_category" id="nm_category" class="form-control" value="{{ $category->NM_CATEGORY }}">
                    </div>

                    <div class="col-md-6 col-sm-6 form-group">
                        <label>Descrição</label>
                        <input type="text" name="ds_category" id="ds_category" class="form-control" value="{{ $category->DS_CATEGORY }}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 form-group d-flex justify-content-between">
                        <button id="validate-new-category" class="btn btn-success">Editar</button>
                        <a href="{{ route('dashboard.category.new') }}" class="btn btn-danger">Voltar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

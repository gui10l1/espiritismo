@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            Adicionar Categoria
        </div>

        <div class="card-body">
            <form action="{{ route('dashboard.category.store') }}" method="post" id="form">
                @csrf

                <div class="row">
                    <div class="col-md-6 col-sm-6 form-group">
                        <label>Nome</label>
                        <input type="text" name="nm_category" id="nm_category" class="form-control">
                    </div>

                    <div class="col-md-6 col-sm-6 form-group">
                        <label>Descrição</label>
                        <input type="text" name="ds_category" id="ds_category" class="form-control">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 form-group d-flex justify-content-between">
                        <button id="validate-new-category" class="btn btn-success">Salvar</button>
                        <button type="reset" class="btn btn-danger">Apagar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card card-body mt-5 mb-5 list">
        <table id="table" class="display">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Descrição</th>
                <th>Editar</th>
                <th>Excluir</th>
            </tr>
            </thead>

            <tbody>
            @foreach($categories as $item)
                <tr>
                    <td>{{ $item->NM_CATEGORY }}</td>
                    <td>{{ $item->DS_CATEGORY }}</td>
                    <td>
                        <a href="{{ route('dashboard.category.view', \Illuminate\Support\Facades\Crypt::encryptString($item->ID_CATEGORY)) }}">
                            <img src="{{ url('img/pen-icon.png') }}" alt="Editar" width="20" height="20">
                        </a>
                    </td>
                    <td>
                        <button class="btn destroy">
                            <img src="{{ url('img/times-icon.png') }}" alt="Excluir" width="20" height="20">
                            <a href="{{ route('dashboard.category.destroy', \Illuminate\Support\Facades\Crypt::encryptString($item->ID_CATEGORY)) }}">
                            </a>
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

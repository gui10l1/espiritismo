@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            Adicionar News
        </div>

        <div class="card-body">
            <form action="{{ route('dashboard.new.store') }}" method="post" id="form">
                @csrf
                <div class="row">
                    <div class="col-md-12 col-sm-12 form-group">
                        <input type="checkbox" name="news_status" id="news_status" class="switch" value="1">
                        <label for="news_status">Ativo</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 form-group">
                        <label>Título</label>
                        <input type="text" name="news_title" id="news_title" class="form-control">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 form-group">
                        <label>Conteúdo</label>
                        <textarea name="news_content" id="news_content" class="form-control" rows="10"
                                  maxlength="65000"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 form-group d-flex justify-content-between">
                        <button id="validate-new-news" class="btn btn-success">Salvar</button>
                        <button type="reset" class="btn btn-danger">Apagar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card card-body mt-5 mb-5 list">
        <table id="table" class="display">
            <thead>
            <tr>
                <th>Título</th>
                <th>Status</th>
                <th>Editar</th>
                <th>Excluir</th>
            </tr>
            </thead>

            <tbody>
            @foreach($news as $item)
                <tr>
                    <td>{{ $item->NEWS_TITLE }}</td>
                    <td>
                        @if($item->NEWS_STATUS == 1)
                            Ativo
                        @else
                            Não Ativo
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('dashboard.new.view', \Illuminate\Support\Facades\Crypt::encryptString($item->ID_NEWS)) }}">
                            <img src="{{ url('img/pen-icon.png') }}" alt="Editar" width="20" height="20">
                        </a>
                    </td>
                    <td>
                        <button class="btn destroy">
                            <img src="{{ url('img/times-icon.png') }}" alt="Excluir" width="20" height="20">
                            <a href="{{ route('dashboard.new.destroy', \Illuminate\Support\Facades\Crypt::encryptString($item->ID_NEWS)) }}">
                            </a>
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            Editar News
        </div>

        <div class="card-body">
            <form action="{{ route('dashboard.new.edit', \Illuminate\Support\Facades\Crypt::encryptString($news->ID_NEWS)) }}" method="post" id="form">
                @csrf
                <div class="row">
                    <div class="col-md-12 col-sm-12 form-group">
                        <input type="checkbox" name="news_status" id="news_status" class="switch" value="1" @if($news->NEWS_STATUS == 1) checked @endif>
                        <label for="news_status">Ativo</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 form-group">
                        <label>Título</label>
                        <input type="text" name="news_title" id="news_title" class="form-control" value="{{ $news->NEWS_TITLE }}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 form-group">
                        <label>Conteúdo</label>
                        <textarea name="news_content" id="news_content" class="form-control" rows="10"
                                  maxlength="65000">{{ $news->NEWS_CONTENT }}</textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 form-group d-flex justify-content-between">
                        <button id="validate-new-news" class="btn btn-success">Editar</button>
                        <a href="{{ route('dashboard.new.new') }}" class="btn btn-danger">Voltar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

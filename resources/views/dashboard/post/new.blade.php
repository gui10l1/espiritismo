@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            Adicionar postagem
        </div>

        <div class="card-body">
            <form action="{{ route('dashboard.post.store') }}" method="post" enctype="multipart/form-data" id="form">
                @csrf
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Categoria</label>
                        <select name="id_category" id="id_category" class="custom-select">
                            <option value="">Escolha uma opção</option>
                            @foreach($categories as $item)
                                <option value="{{ $item->ID_CATEGORY }}">{{ $item->NM_CATEGORY }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Titulo</label>
                        <textarea name="post_title" id="post_title" class="form-control" cols="30" rows="5" maxlength="300"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Sub-Titulo</label>
                        <textarea name="post_subtitle" id="post_subtitle" class="form-control" cols="30" rows="5" maxlength="300"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Conteúdo</label>
                        <div>
                            <div id="toolbar"></div>
                            <div class="editor" style="height: 600px"></div>
                            <textarea name="post_content" id="post_content" class="d-none"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <div class="dropzone">
                            <label>Thumbnail</label>
                            <div class="dropzone-content">
                                <span id="drag-n-drop-2">Arraste o arquivo para cá</span>
                                <strong><span id="file-name-1"></span></strong>
                                <input type="file" name="thumbnail" id="thumbnail" class="input-file-custom">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row d-none">
                    <div class="form-group col-md-12">
                        <label>Pré visualização Thumbnail</label> <br>
                        <img src="" alt="Thumbnail" class="thumbnail" id="preview_thumbnail" style="max-width: 400px; height: auto; border-radius: 8px">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <div class="dropzone">
                            <label>Foto Principal</label>
                            <div class="dropzone-content">
                                <span id="drag-n-drop-1">Arraste o arquivo para cá</span>
                                <strong><span id="file-name-2"></span></strong>
                                <input type="file" name="ft_main_post" id="ft_main_post" class="input-file-custom">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row d-none">
                    <div class="form-group col-md-12">
                        <label>Pré visualização Foto Principal</label> <br>
                        <img src="" alt="Main Photo" id="main-photo" style="width: 100%; border-radius: 10px">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <button id="validate-new-post" type="button" class="btn btn-success">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            ['blockquote', 'code-block'],

            [{'header': 1}, {'header': 2}],               // custom button values
            [{'list': 'ordered'}, {'list': 'bullet'}],
            [{'script': 'sub'}, {'script': 'super'}],      // superscript/subscript
            [{'indent': '-1'}, {'indent': '+1'}],          // outdent/indent
            [{'direction': 'rtl'}],                         // text direction

            [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
            [{'header': [1, 2, 3, 4, 5, 6, false]}],

            [{'color': []}, {'background': []}],          // dropdown with defaults from theme
            [{'font': []}],
            [{'align': []}],

            ['clean']                                         // remove formatting button
        ];
        var options = {
            modules: {
                syntax: true,
                toolbar: toolbarOptions
            },
            placeholder: 'Conteúdo aqui',
            theme: 'snow'
        };
        var editor = new Quill('.editor', options);
    </script>
@endsection

@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            Usuários
        </div>

        <div class="card-body list">
            <table id="table" class="display">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Status</th>
                </tr>
                </thead>

                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->NM_USER }}</td>
                        <td>{{ $user->EMAIL }}</td>
                        <td>
                            @if($user->FL_ACCOUNT_ACTIVATE == 1)
                                Ativo
                            @else
                                Não Ativo
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

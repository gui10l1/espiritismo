@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            Editar Rede Social
        </div>

        <div class="card-body">
            <form action="{{ route('dashboard.network.edit', \Illuminate\Support\Facades\Crypt::encryptString($network->ID_NETWORK)) }}" method="post"data-browse="" id="form">
                @csrf

                <div class="row">
                    <div class="col-md-12 col-sm-12 form-group">
                        <input type="checkbox" name="fl_network_active" id="fl_network_active" class="switch"
                               value="1" @if($network->FL_NETWORK_ACTIVE == 1) checked @endif>
                        <label for="fl_network_activate">Ativo</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-6 form-group">
                        <label>Nome</label>
                        <input type="text" name="nm_network" id="nm_network" class="form-control" value="{{ $network->NM_NETWORK }}">
                    </div>

                    <div class="col-md-6 col-sm-6 form-group">
                        <label>Link</label>
                        <input type="text" name="link_network" id="link_network" class="form-control" value="{{ $network->LINK_NETWORK }}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 form-group d-flex justify-content-between">
                        <button id="validate-new-network" class="btn btn-success">Editar</button>
                        <a href="{{ route('dashboard.network.new') }}" class="btn btn-danger">Voltar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            Adicionar Rede Social
        </div>

        <div class="card-body">
            <form action="{{ route('dashboard.network.store') }}" method="post" id="form">
                @csrf

                <div class="row">
                    <div class="col-md-12 col-sm-12 form-group">
                        <input type="checkbox" name="fl_network_active" id="fl_network_active" class="switch" value="1">
                        <label for="fl_network_activate">Ativo</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-6 form-group">
                        <label>Nome</label>
                        <input type="text" name="nm_network" id="nm_network" class="form-control">
                    </div>

                    <div class="col-md-6 col-sm-6 form-group">
                        <label>Link</label>
                        <input type="text" name="link_network" id="link_network" class="form-control">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 form-group d-flex justify-content-between">
                        <button id="validate-new-network" class="btn btn-success">Salvar</button>
                        <button type="reset" class="btn btn-danger">Apagar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card card-body mt-5 mb-5 list">
        <table id="table" class="display">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Status</th>
                <th>Link</th>
                <th>Editar</th>
                <th>Excluir</th>
            </tr>
            </thead>

            <tbody>
            @foreach($networks as $item)
                <tr>
                    <td>{{ $item->NM_NETWORK }}</td>
                    <td>
                        @if($item->FL_NETWORK_ACTIVE == 1)
                            Ativo
                        @else
                            Não Ativo
                        @endif
                    </td>
                    <td>{{ $item->LINK_NETWORK }}</td>
                    <td>
                        <a href="{{ route('dashboard.network.view', \Illuminate\Support\Facades\Crypt::encryptString($item->ID_NETWORK)) }}">
                            <img src="{{ url('img/pen-icon.png') }}" alt="Editar" width="20" height="20">
                        </a>
                    </td>
                    <td>
                        <button class="btn destroy">
                            <img src="{{ url('img/times-icon.png') }}" alt="Excluir" width="20" height="20">
                            <a href="{{ route('dashboard.network.destroy', \Illuminate\Support\Facades\Crypt::encryptString($item->ID_NETWORK)) }}">
                            </a>
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

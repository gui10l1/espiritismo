@extends('layouts.front-end.app')
@section('content')
    <section class="content news" id="content">
        <div class="posts-1" id="posts-1">
            <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @if(count($images) > 0)
                        @for($i = 0; $i < count($images); $i++)
                            @if($i == 0)
                                <div class="carousel-item active" data-interval="10000">
                                    <img src="{{ url("storage/".$images[$i]->IMAGE_URL) }}" class="d-block w-100"
                                         alt="..." style="max-width: 450px; max-height: 530px">
                                </div>
                            @else
                                <div class="carousel-item" data-interval="10000">
                                    <img src="{{ url("storage/".$images[$i]->IMAGE_URL) }}" class="d-block w-100"
                                         alt="..."
                                         style="max-width: 450px; max-height: 530px">
                                </div>
                            @endif
                        @endfor
                    @endif
                </div>
                <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Anterior</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Próximo</span>
                </a>
            </div>
            <br>
            @forelse($posts_2 as $item)
                <article class="article">
                    <div class="thumbnail">
                        <div>
                            <a href="{{ route('category.view', \Illuminate\Support\Facades\Crypt::encryptString($item->category->ID_CATEGORY)) }}"
                               class="badge-custom">{{ $item->category->NM_CATEGORY }}</a>
                        </div>
                        <a href="{{ route('post.view', ['id' => $item->ID_POST]) }}">
                            <img src="{{ url("storage/" . "$item->THUMBNAIL") }}" alt="Thumbnail"
                                 class="thumbnail">
                        </a>
                    </div>

                    <div class="article-header">
                        <h3><a href="{{ route('post.view', $item->ID_POST) }}">{{ $item->POST_TITLE }}</a>
                        </h3>

                        <ul class="made-by">
                            <li class="by">por</li>
                            <li class="name-author">{{ $item->user->NM_USER }}</li>
                            <li class="date">{{ date('d/m/Y', strtotime($item->DTHR_INC)) }}</li>
                        </ul>
                    </div>

                    <div class="article-resume">
                        <p>
                            {{ $item->POST_SUBTITLE }}
                        </p>
                    </div>
                </article>
            @empty
            @endforelse
        </div>

        <div class="posts-2">
            @forelse($posts_1 as $item)
                <article class="article">
                    <div class="thumbnail">
                        <div>
                            <a href="{{ route('category.view', \Illuminate\Support\Facades\Crypt::encryptString($item->category->ID_CATEGORY)) }}"
                               class="badge-custom">{{ $item->category->NM_CATEGORY }}</a>
                        </div>
                        <a href="{{ route('post.view', ['id' => $item->ID_POST]) }}">
                            <img src="{{ url("storage/" . "$item->THUMBNAIL") }}" alt="Thumbnail"
                                 class="thumbnail">
                        </a>
                    </div>

                    <div class="article-header">
                        <h3><a href="{{ route('post.view', $item->ID_POST) }}">{{ $item->POST_TITLE }}</a>
                        </h3>

                        <ul class="made-by">
                            <li class="by">por</li>
                            <li class="name-author">{{ $item->user->NM_USER }}</li>
                            <li class="date">{{ date('d/m/Y', strtotime($item->DTHR_INC)) }}</li>
                        </ul>
                    </div>

                    <div class="article-resume">
                        <p>
                            {{ $item->POST_SUBTITLE }}
                        </p>
                    </div>
                </article>
            @empty
            @endforelse
        </div>

        @include('layouts.front-end.sidebar-index')
    </section>

    <section class="content pagination">
        <div style="max-width: 100%; margin: 0 auto">
            {{ $posts->links() }}
        </div>
    </section>
@endsection
@section('scripts')
    {{--    <script>--}}
    {{--        $(window).scroll(function () {--}}
    {{--            const offset_section_content = Math.round($("#content").offset().top)--}}
    {{--            const height_content = getComputedStyle(document.getElementById('content')).height--}}
    {{--            const margin_right = Number.parseInt(getComputedStyle(document.getElementById('content')).marginRight) + 43--}}
    {{--            const category_height = getComputedStyle(document.getElementById('categories')).height--}}
    {{--            const real_height = Number.parseInt(height_content) - Number.parseInt(category_height)--}}

    {{--            console.log($(this).scrollTop() > offset_section_content, $(this).scrollTop() < real_height, Number.parseInt(height_content), Number.parseInt(category_height))--}}

    {{--            if ($(this).scrollTop() > offset_section_content && $(this).scrollTop() < real_height) {--}}
    {{--                $("#categories").addClass('js-class')--}}
    {{--                $("#categories").css({--}}
    {{--                    right: margin_right,--}}
    {{--                })--}}
    {{--            } else if ($(this).scrollTop() < offset_section_content || $(this).scrollTop() > real_height) {--}}
    {{--                $("#categories").removeClass('js-class')--}}
    {{--            }--}}
    {{--        })--}}
    {{--    </script>--}}
@endsection

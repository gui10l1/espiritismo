@extends('layouts.front-end.app')
@section('content')
    <section class="content contact">
        <div class="form-contact">
            <form action="{{ route('contact.send') }}" class="form-news-letter form" method="POST">
                <h3>Preencha o formulário de contato</h3>
                @csrf
                <input type="text" class="input-custom" name="name" placeholder="Seu nome" required>

                <input type="text" name="email" id="email" class="input-custom" placeholder="Seu email" required>

                <input type="text" name="title" id="title" class="input-custom" placeholder="Assunto" required>

                <textarea name="content" id="content" cols="30" rows="10" class="input-custom" placeholder="Insira aqui a sua mensagem..." required></textarea>

                <button type="submit" class="button-custom">Realizar Contato</button>
            </form>
        </div>
    </section>
@endsection

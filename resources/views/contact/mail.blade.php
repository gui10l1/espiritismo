<div
    style="font-family: 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; max-width: 1000px; margin: 0 auto; text-align: center; border: 2px solid #5199f0; border-radius: 15px">
    <div style="text-align: center">
        <h1>Novo Contato!</h1>

        <span>O(a) {{ $contact->name }} deixou a seguinte mensagem:</span>
    </div>

    <div style="background-color: #f9f9f9; padding: 5px; border-radius: 15px; margin: 20px 0">
        <h2>{{ $contact->title }}</h2>

        <p>
            {{ $contact->content }}
        </p>
    </div>

    <ul style=" list-style: none; margin-top: 50px">
        <li>Informações para contato com o usuário:</li>

        <li>{{ $contact->email }}</li>
    </ul>
</div>

@extends('layouts.front-end.app')
@section('content')
    <section class="content post">
        <article class="post-article">
            <div class="main-photo">
                <img src="{{ url("storage/" . "$post->FT_MAIN_POST") }}" alt="Main Photo" class="main-photo">
            </div>

            <div class="entry">
                <a class="badge-custom"
                   href="{{ route('category.view', \Illuminate\Support\Facades\Crypt::encryptString($post->category->ID_CATEGORY)) }}">{{ $post->category->NM_CATEGORY }}</a>

                <h1>{{ $post->POST_TITLE }}</h1>

                <h5 class="text-gray mt-5">
                    {{ $post->POST_SUBTITLE }}
                </h5>

                <ul class="made-by">
                    <li class="by">por</li>
                    <li class="name-author">{{ $post->user->NM_USER }}</li>
                    <li class="date">{{ date('d/m/Y', strtotime($post->DTHR_INC)) }}</li>
                </ul>
            </div>

            <div class="content share-post" id="share-post">
                <span>Compartilhar</span>
                <a href="https://facebook.com" target="_blank">
                    <i class="icon-facebook"></i>
                </a>
                <a href="https://instagram.com" target="_blank">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="https://twitter.com" target="_blank">
                    <i class="icon-twitter"></i>
                </a>
            </div>

            <div class="entry ql-editor ql-snow" id="entry">
                {!! $post->POST_CONTENT !!}
            </div>
        </article>
    </section>

    <section class="content social text-center">
        <span>Compartilhar</span>
        <div class="share-post-bottom">
            <a href="https://facebook.com" target="_blank">
                <i class="icon-facebook"></i>
            </a>
            <a href="https://instagram.com" target="_blank">
                <i class="icon-instagram-1"></i>
            </a>
            <a href="https://twitter.com" target="_blank">
                <i class="icon-twitter"></i>
            </a>
        </div>
    </section>

    <section class="content section-comments">
        <div class="comments">
            <button class="button-custom" id="comment">Comentar <i class="icon-down-open" id="to-rotate"></i></button>

            <div id="form-comment" class="d-none">
                {{--                <form onsubmit="replyComment()" class="form-comment d-none" id="form-post-reply">--}}
                {{--                    @csrf--}}
                {{--                    <h3>Deixe sua resposta</h3>--}}
                {{--                    <textarea name="comment_content" cols="30" rows="10" class="input-custom"--}}
                {{--                              placeholder="Deixe sua resposta"--}}
                {{--                              required></textarea>--}}
                {{--                    <button type="submit" class="button-custom">Responder</button>--}}
                {{--                </form>--}}

                @if(\Illuminate\Support\Facades\Auth::check())
                    <form class="form-comment" id="post-comment-form">
                        @csrf
                        <h3>Faça seu comentário</h3>
                        <textarea name="comment_content" id="comment-textarea" cols="30" rows="10" class="input-custom"
                                  placeholder="Deixe seu comentário"
                                  required></textarea>
                        <button type="submit" class="button-custom" id="post-comment-button">Comentar</button>
                    </form>
                @else
                    <form class="form-comment" action="{{ route('login.attempt') }}"
                          method="POST">
                        @csrf
                        <h3>Faça login para comentar</h3>
                        <input type="hidden" name="back" value="true">
                        <input type="email" name="email" id="email" class="input-custom" placeholder="Seu email"
                               required>
                        <input type="password" name="password" id="password" class="input-custom"
                               placeholder="Sua senha"
                               required>
                        <button type="submit" class="button-custom">Entrar</button>
                        <a href="{{ route('user.new') }}">Registre-se</a>
                    </form>
                @endif

                <div class="comment-section content"></div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script>

        var comments = []

        const container = $('.comment-section')

        function init() {
            getComments()
        }

        function getComments() {
            $.post({
                url: '{{ route('post.comments.get', ['id' => $post]) }}',
                data: {_token: '{{ csrf_token() }}'},
                dataType: 'json',
                success(response) {
                    comments = response
                    renderComments(comments, container)
                    setEvents()
                }
            })
        }

        function renderComments(comments, container) {
            container.append(comments.join(''))
        }

        function postComment() {
            $.post({
                url: '{{ route('comment.store', ['id' => $post->ID_POST]) }}',
                data: $('#post-comment-form').serialize(),
                dataType: 'json',
                success(response) {
                    const new_comment = $(response)
                    container.prepend(new_comment.hide().fadeIn())
                    setEvents()
                },
            })
        }

        function deleteComment(id) {
            $.ajax({
                type: 'DELETE',
                url: '{{ route('comment.delete') }}',
                data: {id: id, _token: '{{ csrf_token() }}'},
                dataType: 'json',
                success() {
                    $(`[data-id=${id}]`).fadeOut(400)
                },
                error() {
                    triggerErrorSwal('Algo deu errado ao tentar remover seu comentário.')
                }
            })
        }

        function editComment(id, newContent) {
            $.ajax({
                method: 'PUT',
                data: {id: id, content: newContent, _token: '{{ csrf_token() }}'},
                dataType: 'json',
                url: '{{ route('comment.edit') }}',
                success() {
                    triggerSuccessSwal('Seu comentário foi editado com sucesso!')
                    $(`[data-id=${id}]`).find('.card-body > p').html(newContent)
                },
                error() {
                    triggerErrorSwal('Ocorreu um erro ao tentar editar seu comentário.')
                }
            })
        }

        function replyComment(commentId, content) {
            $.post({
                url: '{{ route('comment.reply', ['id' => $post->ID_POST]) }}',
                data: {id: commentId, content: content, _token: '{{ csrf_token() }}'},
                dataType: 'json',
                success(response) {
                    const newReplyEl = $(response)
                    $(`[data-id=${commentId}] > .reply-comments`).prepend(newReplyEl.hide().fadeIn())
                    setEvents()
                },
                error() {
                    triggerErrorSwal('Ocorreu um erro ao tentar editar seu comentário.')
                }
            })
        }

        function triggerSuccessSwal(text) {
            Swal.fire({
                icon: 'success',
                title: text
            })
        }

        function triggerErrorSwal(text) {
            Swal.fire({
                icon: 'error',
                title: 'Algo de errado!',
                text: text
            })
        }

        function confirmCommentDeleteReply(id) {
            Swal.fire({
                icon: 'warning',
                title: 'Você está prestes a excluir um comentário permanentemente',
                text: 'Tem certeza que deseja prosseguir?',
                confirmButtonText: 'Excluir',
                showCancelButton: true,
            }).then(function (result) {
                if (result.value) {
                    deleteComment(id)
                }
            })
        }

        function confirmCommentDelete(id) {
            Swal.fire({
                icon: 'warning',
                title: 'Você está prestes a excluir um comentário permanentemente',
                text: 'Tem certeza que deseja prosseguir?',
                confirmButtonText: 'Excluir',
                showCancelButton: true,
            }).then(function (result) {
                if (result.value) {
                    deleteComment(id)
                }
            })
        }

        function cancelEditComment(commentEl) {
            const commentBody = commentEl.find('.card-body')
            const commentFooter = commentEl.find('.card-footer')
            const commentContent = commentBody.children('textarea').html()
            commentBody.html(`<p>${commentContent}</p>`)
            commentFooter.html(`<button class='btn btn-danger btn-outline-primary'>Cancelar</button>`)
        }

        function showEditCommentSwal(commentId, commentContent) {
            Swal.fire({
                showCancelButton: true,
                input: 'textarea',
                inputPlaceholder: 'Escrever um comentário...',
                inputValue: commentContent,
                confirmButtonText: 'Editar'
            }).then(function (result) {
                if (result.value) {
                    editComment(commentId, result.value)
                }
            })
        }

        function showReplyCommentSwal(commentId) {
            Swal.fire({
                showCancelButton: true,
                input: 'textarea',
                inputPlaceholder: 'Deixe sua resposta...',
                confirmButtonText: 'Responder'
            }).then(function (result) {
                if (result.value) {
                    replyComment(commentId, result.value)
                }
            })
        }

        function setEvents() {
            $('.delete-comment-reply').click(function (e) {
                const commentId = $(this).parents('.comment-reply').first().data('id')
                confirmCommentDeleteReply(commentId)
            })
            $('.delete-comment').click(function (e) {
                const commentId = $(this).parents('.user-comment').first().data('id')
                confirmCommentDelete(commentId)
            })
            $('.edit-comment').click(function () {
                const commentEl = $(this).parents('.comment').first()
                showEditCommentSwal(commentEl.data('id'), commentEl.find('.card-body > p').html())
            })
            $('.reply-comment').click(function () {
                const commentEl = $(this).parents('.user-comment').first()
                showReplyCommentSwal(commentEl.data('id'))
            })
        }

        $('#post-comment-button').click(function (e) {
            e.preventDefault()

            if($("#comment-textarea").val() === "") {
                return
            }

            postComment()
        })

        init()
    </script>

{{--    <script>--}}
{{--        $(window).scroll(function () {--}}
{{--            const element = $("#share-post")--}}
{{--            const entry2 = document.getElementsByClassName('entry')[1];--}}
{{--            const height = getComputedStyle(entry2).height--}}

{{--            const realHeight = Math.round(Number.parseInt($("#entry").offset().top))--}}
{{--            Number.parseInt(height);--}}

{{--            if ($(this).scrollTop() > Number.parseInt($("#entry").offset().top) && $(this).scrollTop() < realHeight - 200) {--}}
{{--                element.addClass('fixed-to-top')--}}
{{--            } else if ($(this).scrollTop() > realHeight - 200) {--}}
{{--                element.css({--}}
{{--                    top: realHeight - 200--}}
{{--                })--}}
{{--                element.addClass('fixed-to')--}}
{{--                element.removeClass('fixed-to-top')--}}
{{--            } else if ($(this).scrollTop() < Number.parseInt($("#entry").offset().top)) {--}}
{{--                element.removeClass('fixed-to-top')--}}
{{--                element.addClass('fixed-to')--}}
{{--                element.css({--}}
{{--                    top: Number.parseInt($("#entry").offset().top)--}}
{{--                })--}}
{{--            }--}}
{{--        })--}}
{{--    </script>--}}
@endsection

<div data-id="{{ $comment->ID_COMMENT }}" class="user-comment">
    <div class="photo-name">
        <img src="{{ url('storage/' . $comment->user->USER_PHOTO) }}" alt="Avatar">

        <span>Guilherme Ribeiro Soares</span>

        <ul class="made-by">
            <li class="date">30/08/2020</li>
        </ul>

        @if($comment->user->ID_USER === \Illuminate\Support\Facades\Auth::id())
            <button class="delete-comment"><i class="icon-cancel"></i></button>
        @endif
    </div>

    <p>{{ $comment->COMMENT_CONTENT }}</p>

    <div class="reply-comments">
        @if($comment->hasReplies())
            @foreach($comment->replies()->orderBy('DTHR_INC', 'desc')->get() as $reply)
                {!! view('post.comment.replyTemplate', ['reply' => $reply])->render() !!}
            @endforeach
        @endif
    </div>

    @auth()
        <button class="button-reply reply-comment mb-4">Responder</button>
    @endauth
</div>

<script>
    $(".post-reply").click(function () {
        if ($("#post-comment-form").hasClass('d-none')) {
            $("#post-comment-form").removeClass('d-none').hide()
            $("#form-post-reply").addClass('d-none')
            $(".post-reply").html('Responder')
            $("#post-comment-form").fadeIn(400)
        } else {
            $("#post-comment-form").fadeOut(100, function () {
                $(this).addClass('d-none')
                $("#form-post-reply").removeClass('d-none')
                $(".post-reply").html('Comentar')
            })
        }
    })
</script>

{{--<div class="comment-principal comment mt-2">--}}
{{--    <div class="card">--}}
{{--        <div class="card-header d-flex justify-content-between">--}}
{{--            <div>--}}
{{--                {{ $comment->user->NM_USER }}--}}
{{--                <span class="text-secondary">{{ date('d/m/Y H:i', strtotime($comment->DTHR_INC)) }}</span>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--        <div class="card-body">--}}
{{--            <p>{{ $comment->COMMENT_CONTENT }}</p>--}}
{{--        </div>--}}
{{--        @auth()--}}
{{--            <div class="card-footer d-flex flex-row">--}}
{{--                <button class="btn btn-outline-primary reply-comment">Responder</button>--}}
{{--            </div>--}}
{{--        @endauth--}}
{{--    </div>--}}

{{--    <div class="reply-comments">--}}
{{--        @if($comment->hasReplies())--}}
{{--            @foreach($comment->replies()->orderBy('DTHR_INC', 'desc')->get() as $reply)--}}
{{--                {!! view('post.comment.replyTemplate', ['reply' => $reply])->render() !!}--}}
{{--            @endforeach--}}
{{--        @endif--}}
{{--    </div>--}}

{{--</div>--}}

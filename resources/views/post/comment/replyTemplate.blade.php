<div class="comment-reply comment mt-2 ml-5" data-id="{{ $reply->ID_COMMENT }}">
    <hr>

    <div class="photo-name">
        <img src="{{ url('storage/' . $reply->user->USER_PHOTO) }}" alt="Avatar">

        <span>{{ $reply->user->NM_USER }}</span>

        <ul class="made-by">
            <li class="date">{{ date('d/m/Y', strtotime($reply->DTHR_INC)) }}</li>
        </ul>

        @if($reply->user->ID_USER === \Illuminate\Support\Facades\Auth::id())
            <button class="delete-comment-reply"><i class="icon-cancel"></i></button>
        @endif
    </div>

    <p>{{ $reply->COMMENT_CONTENT }}</p>



{{--    <div class="card">--}}
{{--        <div class="card-header d-flex justify-content-between">--}}
{{--            <div>--}}
{{--                {{ $reply->user->NM_USER }} <span class="text-secondary">{{ date('d/m/Y H:i', strtotime($reply->DTHR_INC)) }}</span>--}}
{{--            </div>--}}
{{--            @if($reply->user->ID_USER === \Illuminate\Support\Facades\Auth::id())--}}
{{--                <div>--}}
{{--                    <button class="btn btn-sm btn-primary edit-comment"><i class="fa fa-edit"></i></button>--}}
{{--                    <button class="btn btn-sm btn-danger delete-comment"><i class="fa fa-trash"></i></button>--}}
{{--                </div>--}}
{{--            @endif--}}
{{--        </div>--}}
{{--        <div class="card-body">--}}
{{--            <p>{{ $reply->COMMENT_CONTENT }}</p>--}}
{{--        </div>--}}
{{--    </div>--}}
</div>

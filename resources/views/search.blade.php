@extends('layouts.front-end.app')
@section('content')
    <section class="content mt-5 mb-5">
        <h1><em>Resultado de pesquisa para: {{ $search }}</em></h1>
    </section>

    <section class="content posts-category">
        <div class="posts-1">
            @forelse($results as $result)
                <article class="article">
                    <div class="thumbnail">
                        <div>
                            <a href="#" class="badge-custom">{{ $result->category->NM_CATEGORY }}</a>
                        </div>
                        <a href="{{ route('post.view', ['id' => $result->ID_POST]) }}">
                            <img src="{{ url("storage/" . "$result->THUMBNAIL") }}" alt="Thumbnail"
                                 class="thumbnail">
                        </a>
                    </div>

                    <div class="article-header">
                        <h3><a href="{{ route('post.view', $result->ID_POST) }}">{{ $result->POST_TITLE }}</a>
                        </h3>

                        <ul class="made-by">
                            <li class="by">por</li>
                            <li class="name-author">{{ $result->user->NM_USER }}</li>
                            <li class="date">{{ date('d/m/Y', strtotime($result->DTHR_INC)) }}</li>
                        </ul>
                    </div>

                    <div class="article-resume">
                        <p>
                            {{ $result->POST_SUBTITLE }}
                        </p>
                    </div>
                </article>
            @empty
                <h3>Nenhum resultado foi encontrado para {{ $search }}</h3>
            @endforelse
        </div>

        @include('layouts.front-end.sidebar-index')
    </section>

    <section class="content pagination">
        <div style="max-width: 100%; margin: 0 auto">
            {{ $results->links() }}
        </div>
    </section>
@endsection

<div class="d-none search-form" id="search-form">
    <form action="{{ route('site') }}" method="get">
        <h1 class="text-center text-white mb-4">Faça sua Pesquisa</h1>
        <input type="text" placeholder="Sua pesquisa" name="s" id="s" required class="input-custom">
        <button class="button-custom">Pesquisar</button>
    </form>
</div>
<section class="background d-none" id="background-search"></section>

<header class="header">
    <nav>
        <ul class="menu">
            <li class="menu-icon menu-mobile" id="menu-icon">
                <i class="icon-menu"></i>
            </li>
            <li class="menu-item"><a href="{{ route('site') }}">Home</a></li>
            <li class="menu-item"><a href="{{ route('contact.new') }}">Contato</a></li>
            <li class="menu-brand">
                <a href="{{ route('site') }}">
                    <img class="img-fluid" src="{{ asset('assets/images/espiritismo-luz-fin.png') }}" alt="Logo" width="100" height="100">
                </a>
            </li>
            <li class="menu-item">
                Categorias <i class="icon-down-open"></i>
                <ul class="menu-inside">
                    @foreach($categories as $category)
                        <li>
                            <a href="{{ route('category.view', \Illuminate\Support\Facades\Crypt::encryptString($category->ID_CATEGORY)) }}">{{ $category->NM_CATEGORY }}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
            <li class="menu-item">
                @if(\Illuminate\Support\Facades\Auth::check())
                    <img src="{{ url('storage/' . \Illuminate\Support\Facades\Auth::user()->USER_PHOTO ) }}"
                         alt="Avatar" class="avatar-header" width="70" height="70">
                @else
                    Entrar <i class="icon-down-open"></i>
                @endif
                <ul class="menu-inside">
                    @if(\Illuminate\Support\Facades\Auth::check())
                        <li>
                            <a href="{{ route('user.config', \Illuminate\Support\Facades\Crypt::encryptString(\Illuminate\Support\Facades\Auth::id())) }}">Configurações</a>
                        </li>
                        <li><a href="{{ route('logout') }}">Sair</a></li>
                        @can('isAdmin')
                            <li><a href="{{ route('home') }}">Painel de Controle</a></li>
                        @endcan
                    @else
                        <li><a href="{{ route('login') }}">Entrar</a></li>
                        <li><a href="{{ route('user.new') }}">Cadastrar</a></li>
                    @endif
                </ul>
            </li>
            <li class="menu-icon menu-mobile" id="search">
                <i class="icon-search"></i>
            </li>
        </ul>
    </nav>
</header>

<div class="aside-menu d-none" id="aside-menu">
    <ul class="menu-aside text-center">
        <li class="menu-aside-item menu-brand"><a href="{{ route('site') }}">Espiritismo é Luz</a></li>

        @if(count($categories) > 0) <li class="menu-aside-item"><h3>Categorias</h3></li> @endif

        @foreach($categories as $category)
            <li class="menu-aside-item"><a
                    href="{{ route('category.view', \Illuminate\Support\Facades\Crypt::encryptString($category->ID_CATEGORY)) }}">{{ $category->NM_CATEGORY }}</a></li>
        @endforeach

        <li class="menu-aside-item text-center"><h3>Seu Perfil</h3></li>

        @if(\Illuminate\Support\Facades\Auth::check())
            <li class="menu-aside-item"><a
                    href="{{ route('user.config', \Illuminate\Support\Facades\Crypt::encryptString(\Illuminate\Support\Facades\Auth::id())) }}">Configurações</a>
            </li>
            @can('isAdmin')
                <li class="menu-aside-item"><a href="{{ route('home') }}">Painel de Controle</a></li>
            @endcan
            <li class="menu-aside-item"><a href="{{ route('logout') }}">Sair</a></li>
        @else
            <li class="menu-aside-item"><a href="{{ route('login') }}">Entrar</a></li>
        @endif
    </ul>
</div>

<div class="background d-none" id="background-aside"></div>

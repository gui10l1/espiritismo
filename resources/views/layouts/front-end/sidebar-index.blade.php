<div class="categories" id="categories">
    <h3 class="bg text-center">Categorias</h3>

    <div class="category-options">
        @foreach($categories as $category)
            <a href="{{ route('category.view', \Illuminate\Support\Facades\Crypt::encryptString($category->ID_CATEGORY)) }}"
               class="badge-custom">{{ $category->NM_CATEGORY }}</a>
        @endforeach
    </div>

    <div class="form-news-letter">
        <h3>Inscreva-se e não perca nada!</h3>
        <form action="{{ route('newsletter') }}" method="post" id="form">
            @csrf
            <input class="input-custom" type="text" name="name" placeholder="Seu nome" required>
            <input class="input-custom" type="email" name="email" placeholder="Seu email" required>
            <button class="button-custom" type="submit">Enviar</button>
        </form>
    </div>

    <h3 class="bg text-center">Novidades</h3>

    @foreach($news as $item)
        <div class="blog-item bg">
            <div class="info">
                <span class="category">{{ \App\Helper\AppHelper::dates(date('m', strtotime($item->DTHR_INC))) }} DE {{ date('Y') }}</span>

                <h3 style="font-size: 1.125rem">{{ $item->NEWS_TITLE }}</h3>

                <p class="mt-3">
                    {{ $item->NEWS_CONTENT }}
                </p>

                <span class="date">{{ date('d/m/Y', strtotime($item->DTHR_INC)) }}</span>
            </div>
        </div>
    @endforeach
</div>

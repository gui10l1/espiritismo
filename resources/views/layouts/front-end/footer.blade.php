<footer>
    <section class="content">
{{--        <div class="blog">--}}
{{--            <h3>BLOG</h3>--}}

{{--            <div class="blog-item">--}}
{{--                <div class="thumbnail">--}}
{{--                    <img src="{{ asset('assets/images/thumbnail.png') }}" alt="Thumbnail" class="">--}}
{{--                </div>--}}

{{--                <div class="info">--}}
{{--                    <span class="category">BLOG</span>--}}

{{--                    <p>--}}
{{--                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Reiciendis, laboriosam, assumenda.--}}
{{--                    </p>--}}

{{--                    <span class="date">28 DE AGOSTO 2020</span>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="blog-item">--}}
{{--                <div class="thumbnail">--}}
{{--                    <img src="{{ asset('assets/images/thumbnail.png') }}" alt="Thumbnail" class="">--}}
{{--                </div>--}}

{{--                <div class="info">--}}
{{--                    <span class="category">BLOG</span>--}}

{{--                    <p>--}}
{{--                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Reiciendis, laboriosam, assumenda.--}}
{{--                    </p>--}}

{{--                    <span class="date">28 DE AGOSTO 2020</span>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="blog-item">--}}
{{--                <div class="thumbnail">--}}
{{--                    <img src="{{ asset('assets/images/thumbnail.png') }}" alt="Thumbnail" class="">--}}
{{--                </div>--}}

{{--                <div class="info">--}}
{{--                    <span class="category">BLOG</span>--}}

{{--                    <p>--}}
{{--                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Reiciendis, laboriosam, assumenda.--}}
{{--                    </p>--}}

{{--                    <span class="date">28 DE AGOSTO 2020</span>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="blog-item">--}}
{{--                <div class="thumbnail">--}}
{{--                    <img src="{{ asset('assets/images/thumbnail.png') }}" alt="Thumbnail" class="">--}}
{{--                </div>--}}

{{--                <div class="info">--}}
{{--                    <span class="category">BLOG</span>--}}

{{--                    <p>--}}
{{--                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Reiciendis, laboriosam, assumenda.--}}
{{--                    </p>--}}

{{--                    <span class="date">28 DE AGOSTO 2020</span>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="mt-5">
            <h1 class="bg text-center">Novidades</h1>

            @foreach($news as $item)
                <div class="blog-item">
                    <div class="info text-center w-100">
                        <span class="category">{{ \App\Helper\AppHelper::dates(date('m', strtotime($item->DTHR_INC))) }} DE {{ date('Y') }}</span>

                        <h3 style="font-size: 1.125rem" class="mt-4">{{ $item->NEWS_TITLE }}</h3>

                        <p class="text-center p-3">
                            {{ $item->NEWS_CONTENT }}
                        </p>

                        <span class="date">{{ date('d/m/Y', strtotime($item->DTHR_INC)) }}</span>
                    </div>
                </div>
            @endforeach
        </div>
    </section>

    <section class="scetion-form-footer content">
        <div class="form-news-letter">
            <h3>Inscreva-se e não perca nada!</h3>
            <form action="{{ route('newsletter') }}" method="post">
                @csrf
                <input class="input-custom" type="text" name="name" placeholder="Seu nome" required>
                <input class="input-custom" type="email" name="email" placeholder="Seu email" required>
                <button class="button-custom" type="submit">Enviar</button>
            </form>
        </div>

        <div class="social-medias">
            @foreach($networks as $item)
                <a href="{{ $item->LINK_NETWORK }}" target="_blank">{{ $item->NM_NETWORK }}</a>
            @endforeach
        </div>
    </section>

    <section class="footer">
        <p class="content">
            &copy; Todos os direitos reservados
        </p>
    </section>
</footer>

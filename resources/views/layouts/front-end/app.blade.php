<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Espiritismo é Luz</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,500;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/fonts/fontello/instagram.ttf') }}">
    <!-- Fontello -->
    <link rel="stylesheet" href="{{ asset('assets/css/fontello/instagram.css') }}">
    <!-- Quill -->
    <link rel="stylesheet" href="{{ asset('assets/css/quill/quill-snow.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/quill/quill-bubble.css') }}">
    <!-- Monokai -->
    <link rel="stylesheet" href="{{ asset('assets/css/monokai/monokai.min.css') }}">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap.min.css') }}">
    <!-- My Style -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
</head>
<body>

@include('layouts.front-end.header', ['categories' => \App\Category::all()])

<main>
    @yield('content')
</main>

@include('layouts.front-end.footer')

<script src="{{ asset('assets/js/jquery/jquery.min.js') }}"></script>
<script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
<script src="{{ asset('assets/js/sweet-alert-2/sweet-alert-2.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap/bootstrap.min.js') }}"></script>
@yield('scripts')
<script src="{{ asset('assets/js/script.js') }}"></script>
@include('sweetalert::alert')


</body>
</html>

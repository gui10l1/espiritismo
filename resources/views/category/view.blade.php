@extends('layouts.front-end.app')
@section('content')

    <section class="content mt-5 mb-5">
        <h1><em>{{ $category->NM_CATEGORY }} - {{ $category->DS_CATEGORY }}</em></h1>
    </section>

    <section class="content posts-category">
        <div class="posts-1">
            @forelse($posts as $post)
                <article class="article">
                    <div class="thumbnail">
                        <div>
                            <a href="#" class="badge-custom">{{ $post->category->NM_CATEGORY }}</a>
                        </div>
                        <a href="{{ route('post.view', ['id' => $post->ID_POST]) }}">
                            <img src="{{ url("storage/" . "$post->THUMBNAIL") }}" alt="Thumbnail"
                                 class="thumbnail">
                        </a>
                    </div>

                    <div class="article-header">
                        <h3><a href="{{ route('post.view', $post->ID_POST) }}">{{ $post->POST_TITLE }}</a>
                        </h3>

                        <ul class="made-by">
                            <li class="by">por</li>
                            <li class="name-author">{{ $post->user->NM_USER }}</li>
                            <li class="date">{{ date('d/m/Y', strtotime($post->DTHR_INC)) }}</li>
                        </ul>
                    </div>

                    <div class="article-resume">
                        <p>
                            {{ $post->POST_SUBTITLE }}
                        </p>
                    </div>
                </article>
            @empty
                <h3>Não há postagens nesta categoria</h3>
            @endforelse
        </div>

        @include('layouts.front-end.sidebar-index')
    </section>

    <section class="content pagination">
        <div style="max-width: 100%; margin: 0 auto">
            {{ $posts->links() }}
        </div>
    </section>
@endsection

@extends('layouts.front-end.app')
@section('content')
    <div id="Container" style="background: #fff">
        <form action="{{ route('user.store') }}" method="post" id="form" enctype="multipart/form-data"
              class="form form-news-letter">
            @csrf

            <h3>Cadastro</h3>

            <input type="checkbox" class="switch" id="fl_notification" name="fl_notification" value="1">
            <label for="fl_notification" class="mb-4">Desejo receber notificações por email</label> <br>

            <input type="checkbox" class="switch" id="fl_newsletter" name="fl_newsletter" value="1">
            <label for="fl_newsletter" class="mb-4">Desejo receber um email sempre que houver novas postagens</label>


            <input type="text" class="input-custom" name="nm_user" id="nm_user" placeholder="Seu nome">


            <input type="text" name="email" id="email" class="input-custom" placeholder="Seu email">


            <input type="text" name="rep_email" id="rep_email" class="input-custom" placeholder="Repita seu email">
            <span class="mt-1" id="message-rep-email"></span>

            <input type="password" name="password" id="password" class="input-custom" placeholder="Sua senha">

            <span class="mt-1" id="message-password"></span>


            <input type="password" name="rep_password" id="rep_password" class="input-custom" placeholder="Repita sua senha">
            <span class="mt-1" id="message-rep-password"></span>

            <div class="row">
                <div class="form-group col-md-12" id="drag-and-drop">
                    <div class="dropzone">
                        <div class="dropzone-content">
                            <span id="drag-n-drop">Arraste o arquivo para cá</span>
                            <strong><span id="file-name"></span></strong>
                            <input type="file" name="user_photo" id="user_photo" class="input-file-custom">
                        </div>
                    </div>
                </div>

                <div class="col-md-6 form-group d-none" id="p-photo">
                    <div class="text-center">
                        <img
                            src="{{ url('img/default-avatar.png') }}"
                            class="photo mb-2" height="200" width="200" alt="Foto de perfil"
                            id="preview-user-photo">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 form-group">
                    <button type="button" id="validate-new-user" class="btn btn-success">Realizar Cadastro</button>
                    <button type="reset" class="btn btn-danger">Apagar</button>
                </div>
            </div>
        </form>
    </div>
@endsection

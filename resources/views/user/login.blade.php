@extends('layouts.front-end.app')
@section('content')
    <form action="{{ route('login.attempt') }}" class="form-news-letter " id="form" method="post">
        @csrf

        <div class="row text-justify">
            <div class="col-lg-12 col-sm-12">
                <h1>Login</h1>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-sm-12">
                <input type="text" class="form-control" placeholder="Seu email" name="email" id="email">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-sm-12">
                <input type="password" class="form-control" placeholder="Sua senha" name="password" id="password">
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <button type="button" id="validate-login" class="btn btn-success" style="width: 49%">Entrar</button>
                <a href="{{ route('user.reset-password') }}" class="btn btn-danger" style="width: 49%">Esqueci minha
                    senha</a>
            </div>
        </div>
    </form>
@endsection


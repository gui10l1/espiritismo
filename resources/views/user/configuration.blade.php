@extends('layouts.front-end.app')
@section('content')
    <div class="content">
        <form action="{{ route('user.edit', \Illuminate\Support\Facades\Crypt::encryptString($user->ID_USER)) }}"
              class="form form-news-letter" method="post" enctype="multipart/form-data">
            @csrf

            <h3>Configurações da sua conta</h3>

            <input type="checkbox" class="switch" id="notification"
                   @if($user->FL_NOTIFICATION) checked @endif name="fl_notification" value="1">
            <label for="notification" class="mb-4">Desejo receber notificações por email</label> <br>

            <input type="checkbox" class="switch" id="newsletter"
                   @if($user->FL_NEWSLETTER) checked @endif name="fl_newsletter" value="1">
            <label for="newsletter" class="mb-4">Desejo receber um email sempre que houver novas postagens</label>

            <input type="text" name="nm_user" class="input-custom" placeholder="Seu nome"
                   value="{{ $user->NM_USER }}">

            <input type="password" name="password" id="password" class="input-custom" placeholder="Sua senha">

            <span id="message-password"></span>

            <input type="password" id="rep_password" class="input-custom" placeholder="Repita sua senha">

            <span id="message-rep-password"></span>

            <div class="row">
                <div class="col-md-12" id="drag-and-drop">
                    <div class="dropzone">
                        <div class="dropzone-content">
                            <span id="drag-n-drop">Arraste sua foto para cá</span>
                            <strong><span id="file-name"></span></strong>
                            <input type="file" name="user_photo" id="user_photo" class="input-file-custom">
                        </div>
                    </div>
                </div>

                <div class="col-md-6 d-none photo" id="p-photo">
                    <div class="text-center">
                        <img
                            src=""
                            class="photo mb-2" height="200" width="200" alt="Foto de perfil"
                            id="preview-user-photo">
                    </div>
                </div>
            </div>

            <button type="submit" id="validate-edit-user" class="btn btn-success mt-3">Salvar</button>
            <a href="{{ route('site') }}" type="reset" class="btn btn-danger mt-3">Voltar</a>
        </form>
    </div>
@endsection

@extends('layouts.front-end.app')
@section('content')
    <form
        action="@if(isset($user)) {{ route('user.reset-password-attempt', \Illuminate\Support\Facades\Crypt::encryptString($user->ID_USER)) }} @else {{ route('user.mail-to-reset') }} @endif"
        class="form-news-letter" id="form" method="post">
        @csrf

        <div class="row text-justify">
            <div class="col-lg-12 col-sm-12">
                <h1>Esqueci minha senha</h1>
            </div>
        </div>

        @if(isset($user))
            <div class="form-group">
                <label for="">Nova Senha</label>
                <input type="password" class="form-control" placeholder="Sua nova senha" name="password" id="password">
                <div class="progress mt-2">
                    <div class="progress-bar" id="password-strength" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <span class="mt-1" id="message-password"></span>
            </div>

            <div class="form-group">
                <label>Repetir Senha</label>
                <input type="password" name="rep_password" placeholder="Repetir senha" id="rep_password" class="form-control">
                <span class="my-1" id="message-rep-password"></span>
            </div>
        @else
            <div class="row">
                <div class="form-group col-sm-12">
                    <input type="text" class="form-control" placeholder="Seu email" name="email" id="email">
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <button type="button" id="validate-reset-password" class="btn btn-success" style="width: 49%">
                    Enviar
                </button>
                <a href="{{ route('login') }}" class="btn btn-danger" style="width: 49%">Voltar</a>
            </div>
        </div>
    </form>
@endsection

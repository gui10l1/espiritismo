<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class MailToAuth extends Mailable
{
    use Queueable, SerializesModels;

    private $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Ativação de Conta');
        $this->to($this->user->EMAIL, $this->user->nm_user);
        $route = route('user.activate-my-account', Crypt::encryptString($this->user->ID_USER));

        return $this->view('mail.mail-to-auth', [
            'user' => $this->user,
            'route' => $route
        ]);
    }
}

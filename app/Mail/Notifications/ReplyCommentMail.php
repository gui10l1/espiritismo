<?php

namespace App\Mail\Notifications;

use App\Post;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReplyCommentMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $post;
    private $user;

    public function __construct(User $user, Post $post)
    {
        $this->post = $post;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Alguém respondeu ao seu comentário!');
        $this->to($this->user->EMAIL, $this->user->NM_USER);
        $route = route('post.view', ['id' => $this->post->ID_POST]).'#comment';

        return $this->view('mail.notifications.reply-comment', [
            'post' => $this->post,
            'route' => $route
        ]);
    }
}

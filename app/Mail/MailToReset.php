<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class MailToReset extends Mailable
{
    use Queueable, SerializesModels;

    private $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Resetar minha senha');
        $this->to($this->user->EMAIL, $this->user->NM_USER);
        $route = route('user.view-reset-password', Crypt::encryptString($this->user->ID_USER));

        return $this->view('mail.mail-to-reset', [
            'user' => $this->user,
            'route' => $route
        ]);
    }
}

<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'USERS';

    protected $primaryKey = 'ID_USER';

    protected $fillable = [
        'nm_user',
        'email',
        'password',
        'user_photo',
        'fl_notification',
        'fl_newsletter',
        'tp_user',
        'fl_account_activated',
        'dthr_lastlogin',
        'dthr_inc'
    ];

    public const CREATED_AT = 'DTHR_INC';
    public const UPDATED_AT = 'DTHR_UPD';
}

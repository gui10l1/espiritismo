<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Network extends Model
{
    protected $table = 'NETWORKS';

    protected $primaryKey = 'ID_NETWORK';

    protected $fillable = [
        'nm_network',
        'link_network',
        'fl_network_active'
    ];

    public $timestamps = false;
}

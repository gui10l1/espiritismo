<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'COMMENTS';

    protected $primaryKey = 'ID_COMMENT';

    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo(Post::class, 'ID_POST', 'ID_POST');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'ID_USER', 'ID_USER');
    }

    public function hasReplies()
    {
        return count($this->where('ID_COMMENT_RESP', $this->ID_COMMENT)
            ->get()) > 0;
    }

    public function isReply() {
        return $this->ID_COMMENT_RESP !== null;
    }

    public function replies()
    {
//        return $this->where('ID_COMMENT_RESP', $this->ID_COMMENT);
        return $this->hasMany(Comment::class,
            'ID_COMMENT_RESP', 'ID_COMMENT');
    }


}

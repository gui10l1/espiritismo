<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::check()){
            return view('home');
        }

        return redirect() -> route('login');
    }

    public function site(Request $request)
    {
        if (isset($request->s)) {
            return $this->search($request->s);
        }

        return view('index');
    }

    public function search(string $search) {
        $results = Post::where('POST_TITLE', 'like', "%$search%")
                        ->orWhere('POST_CONTENT', 'like', "%$search%")
                        ->orderBy('DTHR_INC', 'desc')
                        ->paginate(4);

        return view('search', [
            'search' => $search,
            'results' => $results
        ]);
    }
}

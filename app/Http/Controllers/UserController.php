<?php

namespace App\Http\Controllers;

use App\Mail\MailToAuth;
use App\Mail\MailToReset;
use App\User;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    public function loginView()
    {
        return view('user.login');
    }

    public function new()
    {
        return view('user.new');
    }

    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        $user = User::where('EMAIL', $credentials['email'])->first();

        if ($user == null) {
            Alert::error('Credenciais Incorretas', 'Essas credenciais não constam nos nossos registros!');
            if($request->back) {
                return redirect()->back();
            }

            return redirect()->route('login');
        }

        if ($user->FL_ACCOUNT_ACTIVATED != 1) {
            Alert::error('Não Autorizado!', 'Sua conta não foi ativada. Ative sua conta pelo email que acabamos de enviar para sua caixa de entrada!');
            \App\Jobs\MailToAuth::dispatch($user);
            if($request->back) {
                return redirect()->back();
            }

            return redirect()->route('login');
        }

        if (Auth::attempt($credentials)) {
            $user = User::find(Auth::id());

            $user->dthr_lastlogin = new \DateTime();

            $user->save();

            if($request->back) {
                return redirect()->back();
            }

            return redirect()->route('site');
        } else {
            Alert::error('Credenciais Incorretas', 'Essas credenciais não constam nos nossos registros!');
        }

        return redirect()->route('login');
    }

    public function store(Request $request)
    {
       //try {

            $duplicate_user = User::where('EMAIL', $request->email)->first();

            if($duplicate_user != null) {
                \App\Jobs\MailToAuth::dispatch($duplicate_user);
                Alert::warning('Email já cadastrado!', 'Este email já foi cadastrado. Ative sua conta pelo email que enviamos para sua caixa de entrada!');
                return redirect()->route('user.new');
            }

            $user = new User();

            $user->fill($request->except('_token', 'password', 'user_photo', 'fl_newsletter', 'fl_notification'));
            $user->fl_account_activated = 0;
            $user->fl_newsletter = $request->fl_newsletter == null ? 0 : $request->fl_newsletter;
            $user->fl_notification = $request->fl_notification == null ? 0 : $request->fl_notification;
            $user->password = Hash::make($request->password);
            $user->dthr_lastlogin = Date::today();
            $user->user_photo = "Null";

            $user->save();

            $url = env('APP_URL') . "/storage/app/public/users/photo/$user->ID_USER/";
            $path = "users/photo/$user->ID_USER/";

            if ($request->file() != null) {
                $request->file("user_photo")->store($path);

                //$user->user_photo = $url . $request->file("user_photo")->hashName();
                $user->user_photo = $request->file("user_photo")->store($path);
            } else {
                $user->user_photo = "Null";
            }

            $user->save();

            \App\Jobs\MailToAuth::dispatch($user);

            Alert::success('Usuário Cadastrado!', 'Dados cadastrados com sucesso!');

       //} catch (\Exception $e) {
       //    DB::rollBack();
       //    Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
       //}

        return redirect()->route('login');
    }

    public function edit(Request $request, $id)
    {
        try {
            $user = User::find(Crypt::decryptString($id));

            $user->NM_USER = $request->nm_user;
            $user->FL_NOTIFICATION = $request->fl_notification ? $request->fl_notification : 0;
            $user->FL_NEWSLETTER = $request->fl_newsletter ? $request->fl_newsletter : 0;
            $user->password = Hash::make($request->password);

            if ($request->file('user_photo')) {
                Storage::delete($user->USER_PHOTO);
                $user->USER_PHOTO = $request->file('user_photo')->store("users/photo/$user->ID_USER");
            };

            Alert::success('Sucesso!',
                'As configurações de sua conta foram alteradas com sucesso!');

            $user->save();
        } catch (\Exception $e) {
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect()->back();
    }

    public function viewMailToReset()
    {
        return view('user.mail-to-reset');
    }

    public function mailToReset(Request $request)
    {
        $user = User::where('EMAIL', $request->email)->first();

        try {
            if ($user == null) {
                Alert::error('Credenciais Incorretas', 'Essas credenciais não constam nos nossos registros!');
                return redirect()->route('login');
            }

            $user->password = "";

            $user->save();

            \App\Jobs\MailToReset::dispatch($user);

            Alert::success('Email enviado com sucesso!', 'Enviamos um email de verificação para a sua caixa de entrada. Pode ser que isso demore alguns minutos!');
        } catch (\QueryException $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect()->route('login');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('site');
    }

    public function viewToResetPassword($id)
    {
        $user = User::find(Crypt::decryptString($id));

        return view('user.mail-to-reset', compact('user'));
    }

    public function resetPassword(Request $request, $id)
    {
        try {
            $user = User::find(Crypt::decryptString($id));

            $user->PASSWORD = Hash::make($request->password);

            $user->save();

            Alert::success('Senha alterada com sucesso!', 'A sua senha foi alterada com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect()->route('login');
    }

    public function newsletter(Request $request)
    {
        $user = User::where('EMAIL', $request->email)->first();

        try {
            if ($user == null) {
                Alert::error('Email não encontrado!', 'Este email não consta em nossos registros!');
                return redirect()->back();
            }

            $user->FL_NEWSLETTER = 1;

            $user->save();

            Alert::success('Seu email foi cadastrado!', 'Você receberá notificações sempre que houver uma nova postagem!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect()->back();
    }

    public function activateAccount($id)
    {
        try {
            $user = User::find(Crypt::decryptString($id));

            if ($user->FL_ACCOUNT_ACTIVATED) {
                Alert::info('A sua conta já foi ativada!');
            } else {
                $user->FL_ACCOUNT_ACTIVATED = 1;
                $user->save();

                Alert::success('Conta ativada!',
                    'A sua conta foi ativada com sucesso!');
            }
        } catch (DecryptException $e) {
            Alert::error('Erro ao ativar a conta',
                'O link de ativação acessado é inválido!');
        } catch (\Exception $e) {
            Alert::error('Erro',
                'Ocorreu um erro ao realizar a ativação da sua conta. Tente novamente mais tarde.');
        }

        return redirect()->route('login');
    }

    public function config($id)
    {
        $user = User::find(Crypt::decryptString($id));

        return view('user.configuration', compact('user'));
    }
}

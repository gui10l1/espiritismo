<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Mail\Notifications\ReplyCommentMail;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;

class CommentController extends Controller
{
    public function getComments(Post $id)
    {
        if (!Auth::check()){
            $comments = $id->comments()
                ->whereNull('ID_COMMENT_RESP')
                ->orderBy('DTHR_INC', 'desc')
                ->get();
        } else {
            $onlyAuthUserComments = $id->comments()
                ->where('ID_USER', Auth::id())
                ->where(function ($query) {
                    $query->whereNull('ID_COMMENT_RESP');
                })
                ->orderBy('DTHR_INC', 'desc')
                ->get();
            $onlyNonAuthUserComments = $id->comments()
                ->where('ID_USER', '!=', Auth::id())
                ->where(function ($query) {
                    $query->whereNull('ID_COMMENT_RESP');
                })
                ->orderBy('DTHR_INC', 'desc')
                ->get();
            $comments = $onlyAuthUserComments->merge($onlyNonAuthUserComments);
        }

        $renderedComments = [];

        foreach ($comments as $comment) {
            $renderedComment = $this->getRenderedComment($comment);

            array_push($renderedComments, $renderedComment);
        }

        return Response::json($renderedComments);
    }

    private function getRenderedComment(Comment $id, $reply = false) {
        $variable = $reply ? 'reply' : 'comment';

        return view("post.comment.".$variable."Template", [
            $variable => $id
        ])->render();
    }

    public function store(Request $request, Post $id)
    {
        $comment = new Comment();
        $comment->ID_POST = $id->ID_POST;
        $comment->ID_USER = Auth::id();
        $comment->COMMENT_CONTENT = $request->input('comment_content');
        $comment->ID_COMMENT_RESP = 0;
        $comment->save();

        return Response::json($this->getRenderedComment($comment));
    }

    public function delete(Request $request)
    {
        $comment = Comment::find($request->input('id'));

        if ($comment->ID_USER !== Auth::id()) {
            return false;
        }

        if ($comment->hasReplies()) {
            $comment->replies()->delete();
        }

        $comment->delete();

        return true;
    }

    public function deleteReply(Request $request)
    {
        $comment = Comment::find($request->input('id'));

        if ($comment->ID_USER !== Auth::id()) {
            return false;
        }

        $comment->delete();

        return true;
    }

    public function edit(Request $request)
    {
        $comment = Comment::find($request->input('id'));

        if ($comment->ID_USER !== Auth::id()) {
            return false;
        }

        $comment->COMMENT_CONTENT = $request->input('content');

        $comment->save();

        return true;
    }

    public function reply(Request $request, Post $id)
    {
        $commentToReply = Comment::find($request->input('id'));

        $reply = new Comment();
        $reply->COMMENT_CONTENT = $request->input('content');
        $reply->user()->associate(Auth::id());
        $reply->post()->associate($id);
        $reply->ID_COMMENT_RESP = $commentToReply->ID_COMMENT;
        $reply->save();

        if ($commentToReply->user->FL_NOTIFICATION) {
            \App\Jobs\ReplyCommentMail::dispatch($commentToReply, $id);
        }

        return Response::json($this->getRenderedComment($reply, true));
    }

}

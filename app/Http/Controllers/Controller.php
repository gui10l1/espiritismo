<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helper\AppHelper;
use App\Image;
use App\Network;
use App\News;
use App\Post;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    public function __construct()
    {
        $posts1 = [];
        $posts2 = [];
        $news = [];

        $categories = Category::all();
        $networks = Network::where('FL_NETWORK_ACTIVE', 1)->get();
        $posts = Post::orderBy('DTHR_INC', 'desc')->paginate(9);
        $collection = News::where('NEWS_STATUS', 1)->get();
        $images = Image::all();

        foreach ($collection as $item) {
            if(count($news) < 4) {
                $news[] = $item;
            } else {
                return;
            }
        }

        foreach ($posts as $index => $item) {
            if ($index == 0) {
                $posts1[] = $item;
            } elseif ($index % 2 == 0) {
                $posts1[] = $item;
            } else {
                $posts2[] = $item;
            }
        }

        View::share(['networks' => $networks,
            'categories' => $categories,
            'posts_1' => $posts1,
            'posts_2' => $posts2,
            'posts' => $posts,
            'news' => $news,
            'images' => $images
        ]);
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}

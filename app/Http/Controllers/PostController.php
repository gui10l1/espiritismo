<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class PostController extends Controller
{
    public function new()
    {
        return view('post.new');
    }

    public function view($id)
    {
        $post = Post::find($id);

        return view('post.view', compact('post'));
    }
}

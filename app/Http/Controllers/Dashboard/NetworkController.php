<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Network;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use RealRashid\SweetAlert\Facades\Alert;

class NetworkController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    private function checkAbility()
    {
        if(Gate::denies('isAdmin')) {
            abort(403, 'Unauthorized');
        }
    }

    public function new()
    {
        $this->checkAbility();

        $networks = Network::all();

        return view('dashboard.network.new', compact('networks'));
    }

    public function store(Request $request)
    {
        $this->checkAbility();

        try {
            $network = new Network();

            $network->fill($request->except('_token', 'id_user_inc', 'id_user_upd', 'fl_network_activate'));

            $network->fl_network_active = $request->fl_network_active == null ? 0 : $request->fl_network_active;

            $network->save();

            Alert::success('Rede Social Cadastrada!', 'Dados cadastrados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect() -> route('dashboard.network.new');
    }

    public function view($id)
    {
        $this->checkAbility();

        $network = Network::find(Crypt::decryptString($id));

        return view('dashboard.network.view', compact('network'));
    }

    public function edit(Request $request, $id)
    {
        $this->checkAbility();

        try {
            $network = Network::find(Crypt::decryptString($id));

            $network->fill($request->except('_token', 'id_user_upd', 'fl_network_activate'));

            $network->fl_network_active = $request->fl_network_active == null ? 0 : $request->fl_network_active;

            $network->save();

            Alert::success('Rede Social Alterada!', 'Dados alterados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect() -> route('dashboard.network.new');
    }

    public function destroy($id)
    {
        $this->checkAbility();

        try {
            $network = Network::find(Crypt::decryptString($id));

            $network->delete();

            Alert::success('Rede Social Deletada!', 'Dados deletados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect() -> route('dashboard.network.new');
    }
}

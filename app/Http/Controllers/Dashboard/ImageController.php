<?php

namespace App\Http\Controllers\Dashboard;

use App\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use RealRashid\SweetAlert\Facades\Alert;

class ImageController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    private function checkAbility()
    {
        if(Gate::denies('isAdmin')) {
            abort(403, 'Unauthorized');
        }
    }

    public function new()
    {
        $this->checkAbility();

        return view('dashboard.image.new');
    }

    public function store(Request $request)
    {
        $this->checkAbility();

        try {
            $image = new Image();

            $image->dthr_inc = new \DateTime();
            $image->id_user_inc = Auth::id();
            $image->image_url = "Null";

            $image->save();

            $path = "images-index/" . $image->ID_IMAGE;

            $image->image_url = $request->file('image_url')->store($path);

            $image->save();

            Alert::success('Sucesso!', 'Dados cadastrados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect() -> route('dashboard.image.new');
    }
}

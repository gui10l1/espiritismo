<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use PHPUnit\Exception;
use RealRashid\SweetAlert\Facades\Alert;

class CategoryController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    private function checkAbility()
    {
        if(Gate::denies('isAdmin')) {
            abort(403, 'Unauthorized');
        }
    }

    public function new()
    {
        $this->checkAbility();

        $categories = Category::all();

        return view('dashboard.category.new', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->checkAbility();

        try {
            $category = new Category();

            $category->fill($request->except('_token', 'id_user_inc', 'id_user_upd'));
            $category->id_user_upd = Auth::id();
            $category->id_user_inc = Auth::id();

            $category->save();

            Alert::success('Categoria Cadastrada!', 'Dados cadastrados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect() -> route('dashboard.category.new');
    }

    public function view($id)
    {
        $this->checkAbility();

        $category = Category::find(Crypt::decryptString($id));

        return view('dashboard.category.view', compact('category'));
    }

    public function edit(Request $request, $id)
    {
        $this->checkAbility();

        try {
            $category = Category::find(Crypt::decryptString($id));

            $category->fill($request->except('_token', 'id_user_inc', 'id_user_upd'));
            $category->ID_USER_UPD = Auth::id();

            $category->save();

            Alert::success('Categoria Alterada!', 'Dados alterados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect() -> route('dashboard.category.new');
    }

    public function destroy($id)
    {
        $this->checkAbility();

        try {
            $category = Category::find(Crypt::decryptString($id));

            $category->delete();

            Alert::success('Categoria Deletada!', 'Dados deletados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect() -> route('dashboard.category.new');
    }
}

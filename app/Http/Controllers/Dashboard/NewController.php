<?php

namespace App\Http\Controllers\Dashboard;

use App\Helper\AppHelper;
use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use RealRashid\SweetAlert\Facades\Alert;

class NewController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    private function checkAbility()
    {
        if(Gate::denies('isAdmin')) {
            abort(403, 'Unauthorized');
        }
    }

    public function new()
    {
        $this->checkAbility();

        $news = News::all();

        return view('dashboard.new.new', compact('news'));
    }

    public function store(Request $request)
    {
        $this->checkAbility();

        try {
            $news = new News();

            $news->fill($request->except('_token', 'id_user_inc', 'status_news', 'id_user_inc', 'dthr_inc'));

            $news->news_status = $request->news_status == null ? 0 : $request->news_status;
            $news->id_user_inc = Auth::id();
            $news->dthr_inc = new \DateTime();

            $news->save();

            Alert::success('News Cadastrada!', 'Dados cadastrados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect() -> route('dashboard.new.new');
    }

    public function view($id)
    {
        $this->checkAbility();

        $news = News::find(Crypt::decryptString($id));

        return view('dashboard.new.view', compact('news'));
    }

    public function edit(Request $request, $id)
    {
        $this->checkAbility();

        try {
            $news = News::find(Crypt::decryptString($id));

            $news->fill($request->except('_token', 'status_news'));

            $news->news_status = $request->news_status == null ? 0 : $request->news_status;

            $news->save();

            Alert::success('News Alterada!', 'Dados alterados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect() -> route('dashboard.new.new');
    }

    public function destroy($id)
    {
        $this->checkAbility();

        try {
            $news = News::find(Crypt::decryptString($id));

            $news->delete();

            Alert::success('News deletada!', 'Dados deletados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect() -> route('dashboard.new.new');
    }
}

<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    private function checkAbility()
    {
        if(Gate::denies('isAdmin')) {
            abort(403, 'Unauthorized');
        }
    }

    public function list()
    {
        $this->checkAbility();

        $users = User::all();

        return view('dashboard.user.list', compact('users'));
    }

    public function view($id)
    {
        $this->checkAbility();

        $user = User::find(Crypt::decryptString($id));
    }
}

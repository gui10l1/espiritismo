<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Http\Controllers\Controller;
use App\Jobs\Newsletter;
use App\Mail\NewsletterMail;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;

class PostController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    private function checkAbility()
    {
        if (Gate::denies('isAdmin')) {
            abort(403, 'Unauthorized');
        }
    }

    public function new()
    {
        $this->checkAbility();

        $categories = Category::all();

        return view('dashboard.post.new', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->checkAbility();

        try {
            $post = new Post();

            $post->fill($request->except(['_token', 'ft_main_post', 'thumbnail']));

            $post->id_user_inc = Auth::id();
            $post->id_user_upd = Auth::id();
            $post->thumbnail = "Null";
            $post->ft_main_post = "Null";

            $post->save();

            if ($request->file('thumbnail')) {
                $post->thumbnail = $request->file('thumbnail')->store("posts/thumbnail/$post->ID_POST");
            }

            if ($request->file('ft_main_post')) {
                $post->ft_main_post = $request->file('ft_main_post')->store("posts/main_photo/$post->ID_POST");
            }

            $post->save();

            foreach (User::where('FL_NEWSLETTER', '1')->get() as $user) {
                Newsletter::dispatch($user, $post);
            }

            Alert::success('Postagem Cadastrada!', 'Dados cadastrados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect()->route('dashboard.post.new');
    }

    public
    function view($id)
    {
        $this->checkAbility();

        $post = Post::find(Crypt::decryptString($id));

        return view('dashboard.post.view', compact('post'));
    }

    public
    function edit(Request $request, $id)
    {
        $this->checkAbility();

        try {
            $post = Post::find(Crypt::decryptString($id));

            $post->fill($request->except('_token'));

            $post->id_user_inc = Auth::id();
            $post->id_user_upd = Auth::id();

            Alert::success('Postagem Alterada!', 'Dados alterados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect()->route('dashboard.post.new');
    }

    public
    function destroy($id)
    {
        $this->checkAbility();

        try {
            $post = Post::find(Crypt::decryptString($id));

            $post->delete();

            Alert::success('Postagem Deletada!', 'Dados deletados com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            Alert::error('Um erro inesperado aconteceu!', "Pedimos desculpa pelo transtorno. Tente novamente mais tarde, e se o erro persistir, contate o administrador do sistema!");
        }

        return redirect()->route('dashboard.post.new');
    }
}

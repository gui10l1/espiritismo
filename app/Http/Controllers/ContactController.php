<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Mail\MailContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function new()
    {
        return view('contact.new');
    }

    public function send(Request $request)
    {
        try {
            $contact = new Contact();

            $contact->fill($request->except('_token'));

            \App\Jobs\Contact::dispatch($contact);
        } catch (\Exception $e) {
            return $e;
        }

        return redirect() -> route('site');
    }
}

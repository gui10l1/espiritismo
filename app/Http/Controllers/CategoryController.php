<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class CategoryController extends Controller
{

    public function view($id)
    {
        $category = Category::find(Crypt::decryptString($id));

        $posts = $category->posts()->paginate(4);

        return view('category.view', [
            'category' => $category,
            'posts' => $posts
        ]);

    }

}

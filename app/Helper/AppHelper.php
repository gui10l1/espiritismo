<?php


namespace App\Helper;


class AppHelper
{
    public static function dateToBr($date)
    {
        //ex: 2010-31-04 para 31/04/2010
        $nDate = implode("/", array_reverse(explode("-", $date)));
        return $nDate;
    }

    public static function dateToMysql($date)
    {
        return implode("-", array_reverse(explode("/", $date)));
    }

    public static function dates($date)
    {
        if ($date == 1) {
            return 'JANEIRO';
        } elseif ($date == 2) {
            return 'FEVEREIRO';
        } elseif ($date == 3) {
            return 'MARÇO';
        } elseif ($date == 4) {
            return 'ABRIL';
        } elseif ($date == 5) {
            return 'MAIO';
        } elseif ($date == 6) {
            return 'JUNHO';
        } elseif ($date == 7) {
            return 'JULHO';
        } elseif ($date == 8) {
            return 'AGOSTO';
        } elseif ($date == 9) {
            return 'SETEMBRO';
        } elseif ($date == 10) {
            return 'OUTUBRO';
        } elseif ($date == 11) {
            return 'NOVEMBRO';
        } else {
            return 'DEZEMBRO';
        }
    }
}

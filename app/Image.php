<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'IMAGES';

    protected $primaryKey = 'ID_IMAGE';

    protected $fillable = [
        'image_url',
        'id_user_inc',
        'dthr_inc'
    ];

    public $timestamps = false;
}

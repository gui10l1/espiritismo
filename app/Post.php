<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'POSTS';

    protected $primaryKey = 'ID_POST';

    protected $fillable = [
        'id_category',
        'post_content',
        'post_title',
        'post_subtitle',
        'id_user_inc',
        'id_user_upd',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'ID_USER_INC', 'ID_USER');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'ID_CATEGORY', 'ID_CATEGORY');
    }

    public function comments()
    {
        return Comment::where('ID_POST', $this->ID_POST);
    }

    public const CREATED_AT = 'DTHR_INC';
    public const UPDATED_AT = 'DTHR_UPD';
}

<?php

namespace App\Jobs;

use App\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;

class ReplyCommentMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    private $commentToReply;
    private $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Comment $comment, $id)
    {
        $this->commentToReply = $comment;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send(new \App\Mail\Notifications\ReplyCommentMail($this->commentToReply->user()->first(), $this->id));
        Artisan::call('queue:work');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'NEWS';
    protected $primaryKey = 'ID_NEWS';

    protected $fillable = [
        'news_title',
        'news_content',
        'news_status',
        'id_user_inc',
        'dthr_inc'
    ];

    public $timestamps = false;
}

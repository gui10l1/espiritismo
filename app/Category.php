<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $table = 'CATEGORIES';

    protected $primaryKey = 'ID_CATEGORY';

    protected $fillable = [
        'nm_category',
        'ds_category',
        'id_user_inc',
        'id_user_upd'
    ];

    public function posts()
    {
        return Post::where('ID_CATEGORY', $this->ID_CATEGORY);
    }

    public const CREATED_AT = 'DTHR_INC';
    public const UPDATED_AT = 'DTHR_UPD';
}

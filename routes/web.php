<?php

use App\Post;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/painel-de-controle', 'HomeController@index')->name('index');

Route::get('/', 'HomeController@site')->name('site');

Auth::routes();

Route::get('/painel-de-controle', 'HomeController@index')->name('home');


//---------------------------------------------------------DASHBOARD----------------------------------------------------


Route::group(['namespace' => 'Dashboard'], function () {
//user
    Route::get('/user/list', 'UserController@list')->name('user.list');
    Route::get('/dashboard/user/view/{id}', 'UserController@view')->name('dashboard.user.view');
    Route::get('/dashboard/user/destroy/{id}', 'UserController@destroy')->name('dashboard.user.destroy');

    Route::post('/dashboard/user/edit/{id}', 'UserController@edit')->name('dashboard.user.edit');

//post
    Route::get('/post/new', 'PostController@new')->name('dashboard.post.new');
//    Route::get('/post/view/{id}', 'PostController@view')->name('dashboard.post.view');
    Route::get('/post/destroy/{id}', 'PostController@destroy')->name('dashboard.post.destroy');

    Route::post('/post/edit/{id}', 'PostController@edit')->name('dashboard.post.edit');
    Route::post('/post/store', 'PostController@store')->name('dashboard.post.store');

//new
    Route::get('/new/new', 'NewController@new')->name('dashboard.new.new');
    Route::get('/new/view/{id}', 'NewController@view')->name('dashboard.new.view');
    Route::get('/new/destroy/{id}', 'NewController@destroy')->name('dashboard.new.destroy');

    Route::post('/new/edit/{id}', 'NewController@edit')->name('dashboard.new.edit');
    Route::post('/new/store', 'NewController@store')->name('dashboard.new.store');

//category
    Route::get('/category/new', 'CategoryController@new')->name('dashboard.category.new');
    Route::get('/category/dashboard/view/{id}', 'CategoryController@view')->name('dashboard.category.view');
    Route::get('/category/destroy/{id}', 'CategoryController@destroy')->name('dashboard.category.destroy');

    Route::post('/category/edit/{id}', 'CategoryController@edit')->name('dashboard.category.edit');
    Route::post('/category/store', 'CategoryController@store')->name('dashboard.category.store');

//network
    Route::get('/network/new', 'NetworkController@new')->name('dashboard.network.new');
    Route::get('/network/view/{id}', 'NetworkController@view')->name('dashboard.network.view');
    Route::get('/network/destroy/{id}', 'NetworkController@destroy')->name('dashboard.network.destroy');

    Route::post('/network/edit/{id}', 'NetworkController@edit')->name('dashboard.network.edit');
    Route::post('/network/store', 'NetworkController@store')->name('dashboard.network.store');

//image
    Route::get('/image/new', 'ImageController@new')->name('dashboard.image.new');

    Route::post('/image/store', 'ImageController@store')->name('dashboard.image.store');
});

//---------------------------------------------------------DASHBOARD----------------------------------------------------

//login
Route::get('/login', 'UserController@loginView')->name('login');
Route::get('/register', 'UserController@register')->name('register');
Route::post('/login/attempt', 'UserController@login')->name('login.attempt');

Route::get('/logout', 'UserController@logout')->name('logout');

//category
Route::get('/category/view/{id}', 'CategoryController@view')->name('category.view');

//user
Route::get('/user/new', 'UserController@new')->name('user.new');
Route::get('/user/reset-password', 'UserController@viewMailToReset')->name('user.reset-password');
Route::get('/user/view/{id}', 'UserController@view')->name('user.view');
Route::get('/user/reset-password/{id}', 'UserController@viewToResetPassword')->name('user.view-reset-password');
Route::get('/user/activate-my-account/{id}', 'UserController@activateAccount')->name('user.activate-my-account');
Route::get('/user/config/{id}', 'UserController@config')->name('user.config');

Route::post('/user/reset-password/{id}', 'UserController@resetPassword')->name('user.reset-password-attempt');
Route::post('/user/store', 'UserController@store')->name('user.store');
Route::post('/user/mail-to-reset', 'UserController@mailToReset')->name('user.mail-to-reset');
Route::post('/user/edit/{id}', 'UserController@edit')->name('user.edit');
Route::post('/user/activate-deactivate/{id}', 'UserController@activateDeactivate')->name('user.activate-deactivate');


Route::post('/newsletter', 'UserController@newsletter')->name('newsletter');

//post

Route::get('/post/view/{id}', 'PostController@view')->name('post.view');

//comment
Route::post('post/{id}/storeComment', 'CommentController@store')->name('comment.store');
Route::post('post/{id}/comments', 'CommentController@getComments')->name('post.comments.get');
Route::delete('comment/delete', 'CommentController@delete')->name('comment.delete');
Route::put('comment/edit', 'CommentController@edit')->name('comment.edit');
Route::post('post/{id}/replayComment', 'CommentController@reply')->name('comment.reply');


//contact

Route::get('/contact', 'ContactController@new')->name('contact.new');

Route::post('/contact/send', 'ContactController@send')->name('contact.send');



<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CATEGORIES', function (Blueprint $table) {
            $table->id('ID_CATEGORY');
            $table->string('NM_CATEGORY');
            $table->string('DS_CATEGORY');
            $table->bigInteger('ID_USER_INC')->unsigned();
            $table->bigInteger('ID_USER_UPD')->unsigned();
            $table->dateTime('DTHR_INC');
            $table->dateTime('DTHR_UPD');

            $table->foreign('ID_USER_INC')->on('users')->references('ID_USER');
            $table->foreign('ID_USER_UPD')->on('users')->references('ID_USER');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cateories');
    }
}

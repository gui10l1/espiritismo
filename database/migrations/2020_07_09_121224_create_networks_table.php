<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('NETWORKS', function (Blueprint $table) {
            $table->id('ID_NETWORK');
            $table->string('NM_NETWORK');
            $table->text('LINK_NETWORK');
            $table->boolean('FL_NETWORK_ACTIVE')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('networks');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('NEWS', function (Blueprint $table) {
            $table->id('ID_NEWS');
            $table->string('NEWS_TITLE');
            $table->text('NEWS_CONTENT');
            $table->string('NEWS_STATUS', 10);
            $table->bigInteger('ID_USER_INC')->unsigned();
            $table->date('DTHR_INC');

            $table->foreign('ID_USER_INC')->on('users')->references('ID_USER');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}

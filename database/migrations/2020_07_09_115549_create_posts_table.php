<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('POSTS', function (Blueprint $table) {
            $table->id('ID_POST');
            $table->bigInteger('ID_CATEGORY')->unsigned();
            $table->text('POST_CONTENT');
            $table->string('POST_TITLE', 300);
            $table->string('POST_SUBTITLE', 300);
            $table->string('THUMBNAIL', 250);
            $table->string('FT_MAIN_POST', 250);
            $table->bigInteger('ID_USER_INC')->unsigned();
            $table->bigInteger('ID_USER_UPD')->unsigned();
            $table->dateTime('DTHR_INC');
            $table->dateTime('DTHR_UPD');

            $table->foreign('ID_USER_INC')->on('users')->references('ID_USER');
            $table->foreign('ID_USER_UPD')->on('users')->references('ID_USER');
            $table->foreign('ID_CATEGORY')->on('categories')->references('ID_CATEGORY');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

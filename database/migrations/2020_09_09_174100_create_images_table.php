<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('IMAGES', function (Blueprint $table) {
            $table->id('ID_IMAGE');
            $table->string('IMAGE_URL', 300);
            $table->unsignedBigInteger('ID_USER_INC');
            $table->dateTime('DTHR_INC');

            $table->foreign('ID_USER_INC')->on('USERS')->references('ID_USER');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('COMMENTS', function (Blueprint $table) {
            $table->id('ID_COMMENT');
            $table->bigInteger('ID_POST')->unsigned();
            $table->bigInteger('ID_USER')->unsigned();
            $table->text('COMMENT_CONTENT');
            $table->foreignId('ID_COMMENT_RESP');
            $table->integer('CURTIDAS')->nullable();

            $table->foreign('ID_POST')->on('posts')->references('ID_POST');
            $table->foreign('ID_USER')->on('users')->references('ID_USER');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}

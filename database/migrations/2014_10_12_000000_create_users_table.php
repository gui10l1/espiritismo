<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('USERS', function (Blueprint $table) {
            $table->id('ID_USER');
            $table->string('NM_USER');
            $table->string('EMAIL')->unique();
            $table->string('password');
            $table->string('USER_PHOTO');
            $table->boolean('FL_NOTIFICATION');
            $table->boolean('FL_NEWSLETTER');
            $table->enum('TP_USER', ['sistema', 'blog'])->default('blog');
            $table->boolean('FL_ACCOUNT_ACTIVATED');
            $table->datetime('DTHR_LASTLOGIN');
            $table->date('DTHR_INC');
            $table->date('DTHR_UPD');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

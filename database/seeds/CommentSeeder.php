<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 100; $i++) {
            $user = \App\User::inRandomOrder()->first();
            $post = \App\Post::inRandomOrder()->first();

            $comment = new \App\Comment();
            $comment->COMMENT_CONTENT = \Illuminate\Support\Str::random(100);
            $comment->user()->associate($user);
            $comment->post()->associate($post);
            $comment->save();
        }
    }
}

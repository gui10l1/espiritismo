<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 50; $i++) {
            $category = \App\Category::inRandomOrder()->first();
            $user = \App\User::inRandomOrder()->first();

            $post = new \App\Post();
            $post->ID_CATEGORY = $category->ID_CATEGORY;
            $post->POST_CONTENT = $this->generateRandomContent();
            $post->POST_TITLE = 'Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Quibusdam necessitatibus porro nostrum, inventore cum!';
            $post->POST_SUBTITLE = \Illuminate\Support\Str::random(50);
            $post->THUMBNAIL = 'posts/thumbnail/1/QCbmtCeKc02W3hmzbHKoT1DWnwi3kkRg9U5FAWIe.png';
            $post->FT_MAIN_POST = 'posts/main_photo/1/QCbmtCeKc02W3hmzbHKoT1DWnwi3kkRg9U5FAWIe.png';
            $post->ID_USER_INC = $user->ID_USER;
            $post->ID_USER_UPD = $user->ID_USER;
            $post->save();
        }
    }

    private function generateRandomContent() {
        $content = '';
        for ($i = 0; $i < rand(50, 100); $i++) {
            $content .= ' '.\Illuminate\Support\Str::random(rand(10, 50));
        }
        return $content;
    }
}

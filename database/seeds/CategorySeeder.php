<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $user = \App\User::inRandomOrder()->first();

            $category = new \App\Category();
            $category->NM_CATEGORY = \Illuminate\Support\Str::random(10);
            $category->DS_CATEGORY = \Illuminate\Support\Str::random(10);
            $category->ID_USER_INC = $user->ID_USER;
            $category->ID_USER_UPD = $user->ID_USER;
            $category->save();
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $credentials = [
            'email' => 'chustrupgamer@gmail.com',
            'password' => \Illuminate\Support\Facades\Hash::make('ASD123/*-'),
            'nm_user' => 'Guilherme',
            'user_photo' => 'Null',
            'fl_notification' => 0,
            'fl_newsletter' => 0,
            'tp_user' => 'sistema',
            'fl_account_activate' => 1,
            'dthr_lastlogin' => new \DateTime(),
            'dthr_inc' => new \DateTime(),
        ];

        $user = new \App\User();
        $user->fill($credentials);
        $user->save();

        for ($i = 0; $i < 10; $i++) {
            $user = new \App\User();

            $credentials = [
                'email' => \Illuminate\Support\Str::random(10).'@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('ASD123/*-'),
                'nm_user' => \Illuminate\Support\Str::random(10),
                'user_photo' => 'Null',
                'fl_notification' => 0,
                'fl_newsletter' => 0,
                'tp_user' => 'blog',
                'fl_account_activate' => 1,
                'dthr_lastlogin' => new \DateTime(),
                'dthr_inc' => new \DateTime(),
            ];

            $user->fill($credentials);
            $user->save();
        }
    }
}
